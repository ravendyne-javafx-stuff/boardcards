## Boardcards Sample App

Simple diagramming application based on [CardsFX](https://gitlab.com/javafx-stuff/com.ravendyne.cardsfx) where you can create, save and load diagrams with two node types and four connector types.


### Dependencies

- [consteallation](https://gitlab.com/ravendyne-javafx-stuff/com.ravendyne.constellation)
- [dynamofx](https://gitlab.com/ravendyne-javafx-stuff/com.ravendyne.dynamofx)
- [cardsfx](https://gitlab.com/ravendyne-javafx-stuff/com.ravendyne.cardsfx)
- [fxtoys](https://gitlab.com/ravendyne-javafx-stuff/com.ravendyne.fxtoys)
