package com.ravendyne.cardsboard.stackofcards.diamonds.deck;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder.ConnectorType;
import com.ravendyne.cardsboard.stackofcards.diamonds.deck.card.DiamondsCardBuilder;
import com.ravendyne.cardsboard.stackofcards.diamonds.deck.card.DiamondsCardBuilder.CardType;
import com.ravendyne.cardsboard.stackofcards.diamonds.deck.pin.DiamondsPinBuilder;

public class DiamondsCreator implements IDefaultsBuilder {
    private static DiamondsCreator instance;
    
    private static ConnectorType defaultConnectorType = ConnectorType.Cubic;
    private static CardType defaultCardType = CardType.SharpDiamond;

    private DiamondsCreator() {}
    
    public static DiamondsCreator getInstance() {
        if(instance == null) {
            instance = new DiamondsCreator();
        }
        
        return instance;
    }

    public DiamondsPinBuilder pin() {
        return new DiamondsPinBuilder();
    }

    public DiamondsCardBuilder card() {
        return new DiamondsCardBuilder();
    }

    public ConnectorBuilder connector() {
        return new ConnectorBuilder();
    }

    public void setDefaultConnectorType(ConnectorType type) {
        defaultConnectorType = type;
    }

    public void setDefaultCardType(CardType type) {
        defaultCardType = type;
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        ICard card = card()
                .setDrawing(drawing)
                .setTitle("'diamond' card")
                .setType(defaultCardType)
                .build();

        pin().setCard(card).setLocation(Location.Left).build();
        pin().setCard(card).setLocation(Location.Right).build();
        pin().setCard(card).setLocation(Location.Top).build();
        pin().setCard(card).setLocation(Location.Bottom).build();
        
        return card;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        IConnector connector = connector()
                .setDrawingBoard(drawing)
                .setSource(source)
                .setTarget(target)
                .setType(defaultConnectorType)
                .build();
        return connector;
    }

}
