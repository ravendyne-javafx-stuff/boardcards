package com.ravendyne.cardsboard.stackofcards.diamonds.deck;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.samples.SampleDrawingPane;

public class DiamondsDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return DiamondsCreator.getInstance();
    }
}
