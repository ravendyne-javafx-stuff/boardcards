package com.ravendyne.cardsboard.stackofcards.diamonds.deck.pin;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

import javafx.scene.paint.Color;

public class DiamondsPinBuilder {

    ICard pCard;
    Color pColor;
    Location pLocation;
    
    public DiamondsPinBuilder() {
        pColor = Color.web("#00a8f4");
    }
    
    public DiamondsPinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public DiamondsPinBuilder setColor(Color color) {
        Objects.requireNonNull(color);
        pColor = color;
        return this;
    }

    public DiamondsPinBuilder setColor(String colorString) {
        pColor = Color.web(colorString);
        return this;
    }

    public DiamondsPinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(pCard);
        Objects.requireNonNull(pColor);
        Objects.requireNonNull(pLocation);

        IPin pin = new MiniCirclePin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }


}
