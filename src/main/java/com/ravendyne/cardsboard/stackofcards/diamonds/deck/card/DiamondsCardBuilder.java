package com.ravendyne.cardsboard.stackofcards.diamonds.deck.card;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;

public class DiamondsCardBuilder {
    private static final boolean bDebug = false;
    
    public static enum CardType {
        SharpDiamond,
        CutDiamond,
    }
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    
    CardType pCardType;
    
    public DiamondsCardBuilder() {
        pCardType = CardType.SharpDiamond;
    }

    public DiamondsCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public DiamondsCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }
    
    public DiamondsCardBuilder setType(CardType cardType) {
        pCardType = cardType;
        return this;
    }

    public ICard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ICard card = null;
        switch(pCardType) {
        case SharpDiamond:
            card = new SimpleDiamondCard( pDrawing, pTitle );
            break;
        case CutDiamond:
            card = new CutDiamondCard( pDrawing, pTitle );
            break;
        }
        card.buildUI();
        pDrawing.addCard(card);
        if(bDebug) System.out.println("added card #" + card.getCardId());

        return card;
    }

}
