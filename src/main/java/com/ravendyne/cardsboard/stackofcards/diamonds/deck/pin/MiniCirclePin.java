package com.ravendyne.cardsboard.stackofcards.diamonds.deck.pin;


import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.drawing.AbstractPin;

import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.shape.Circle;

public class MiniCirclePin extends AbstractPin {

    public static final double connectorSize = 5;
    
    MiniCirclePin(ICard card) {
        super( card );
    }

    @Override
    protected Region createRootUINode() {
        HBox pinUI = new HBox();
//        pinUI.getStyleClass().add( "debug-region" );

        pinUI.setAlignment(Pos.CENTER);
        
        pinUI.getChildren().add(getConnectorAttachmentNode());

        // we have to set width/height on pin in this case,
        // otherwise pin's HBox root container will get auto-stretched
        // to the card width/height and Circle we use for connector
        // attachment point UI will not be rendered in its intended
        // place
        pinUI.setMaxSize(connectorSize, connectorSize);
        
        return pinUI;
    }
    
    @Override
    protected Node createConnectionPoinUINode() {
        return new Circle(connectorSize);
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[id= " + getPinId() + ", location=" + getLocation() + ", parent=" + getCard().getCardId() + "]";
    }

    @Override
    public Point2D getAttachmentPositionLocal() {
        // (0,0) is position of circle center in its local coordinates
        // and we want connection to attach to circle center
        return Point2D.ZERO;
    }
}
