package com.ravendyne.cardsboard.stackofcards.diamonds.deck.card;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.drawing.AbstractCard;
import com.ravendyne.cardsboard.stackofcards.diamonds.deck.pin.MiniCirclePin;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class CutDiamondCard extends AbstractCard implements ITitledCard {
    
    Label titleLabel;
    
    IPin topPin;
    IPin bottomPin;
    IPin leftPin;
    IPin rightPin;
    
    final double diamondWidth = 50;
    final double diamondHeight = 75;

    private StackPane uiNode;

    CutDiamondCard(IDrawingBoard drawing, String title) {
        super(drawing);
        
        titleLabel = new Label( title );
    }
    
    private StackPane getUINode() {
        if(uiNode == null) {
            StackPane cardPane = new StackPane();
            
            Path diamond = new Path();
            diamond.getElements().add( new MoveTo( 0.2 * diamondWidth, 0.0 * diamondHeight ) );
            diamond.getElements().add( new LineTo( 0.8 * diamondWidth, 0.0 * diamondHeight ) );
            diamond.getElements().add( new LineTo( 1.0 * diamondWidth, 0.5 * diamondHeight ) );
            diamond.getElements().add( new LineTo( 0.8 * diamondWidth, 1.0 * diamondHeight ) );
            diamond.getElements().add( new LineTo( 0.2 * diamondWidth, 1.0 * diamondHeight ) );
            diamond.getElements().add( new LineTo( 0.0 * diamondWidth, 0.5 * diamondHeight ) );
            diamond.getElements().add( new ClosePath() );

            cardPane.setShape( diamond );
            cardPane.setScaleShape( true );
            cardPane.setCenterShape( true );

            VBox contentBox = new VBox();
            contentBox.setPadding( new Insets( 15 ) );

            contentBox.getChildren().add(this.titleLabel);

            cardPane.getChildren().add(contentBox);

            uiNode = cardPane;
        }

        return uiNode;
    }

    @Override
    protected Pane createRootUINode() {
        return getUINode();
    }

    @Override
    protected Node createContentUINode() {
        return getUINode();
    }

    @Override
    public String getTitle() {
        return titleLabel.getText();
    }

    @Override
    public void setTitle(String text) {
        titleLabel.setText(text);
    }

    @Override
    public void discard() {
        super.discard();

        // since we added reference to titleLabel to our root UI node
        // it helps with memory leak debugging to nullify it here
        titleLabel = null;
    }

    @Override
    protected void removePinFromUI( IPin pin ) {
        getRootNode().getChildren().remove( pin.getRootNode() );
    }

    @Override
    protected void addPinToUI(IPin pin) {
        IPin oldPin = null;

        switch(pin.getLocation()) {
        case Top:
            oldPin = topPin;
            topPin = pin;
            break;
        case Bottom:
            oldPin = bottomPin;
            bottomPin = pin;
            break;
        case Left:
            oldPin = leftPin;
            leftPin = pin;
            break;
        case Right:
            oldPin = rightPin;
            rightPin = pin;
            break;
        case Center:
            throw new UnsupportedOperationException("Cant't add pin to card center.");
        }
        
        if(oldPin != null) {
            throw new UnsupportedOperationException("Can't add pin to already populated location");
        }

        setPinLocation(pin);

        getRootNode().getChildren().add( pin.getRootNode() );
    }

    private boolean setPinLocation(IPin pin) {
        switch (pin.getLocation()) {
        case Top:
            StackPane.setAlignment(pin.getRootNode(), Pos.TOP_CENTER);
            pin.getRootNode().setTranslateY( - MiniCirclePin.connectorSize * 2 );
            break;
        case Right:
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_RIGHT);
            pin.getRootNode().setTranslateX( + MiniCirclePin.connectorSize * 2 );
            break;
        case Bottom:
            StackPane.setAlignment(pin.getRootNode(), Pos.BOTTOM_CENTER);
            pin.getRootNode().setTranslateY( + MiniCirclePin.connectorSize * 2 );
            break;
        case Left:
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_LEFT);
            pin.getRootNode().setTranslateX( - MiniCirclePin.connectorSize * 2 );
            break;
        case Center:
            throw new UnsupportedOperationException("Cant't set pin location to card center.");
        }
        
        return true;
    }

}
