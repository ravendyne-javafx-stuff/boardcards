package com.ravendyne.cardsboard.stackofcards.whiteboard.pack.pin;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

import javafx.scene.paint.Color;

public class WhiteboardPinBuilder {

    ICard pCard;
    Color pColor;
    Location pLocation;
    
    public WhiteboardPinBuilder() {
        pColor = Color.web("#00a8f4");
    }
    
    public WhiteboardPinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public WhiteboardPinBuilder setColor(Color color) {
        Objects.requireNonNull(color);
        pColor = color;
        return this;
    }

    public WhiteboardPinBuilder setColor(String colorString) {
        pColor = Color.web(colorString);
        return this;
    }

    public WhiteboardPinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(pCard);
        Objects.requireNonNull(pColor);
        Objects.requireNonNull(pLocation);

        IPin pin = new WhiteboardPin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }


}
