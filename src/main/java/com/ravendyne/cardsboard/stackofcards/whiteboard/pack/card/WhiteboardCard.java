package com.ravendyne.cardsboard.stackofcards.whiteboard.pack.card;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.drawing.AbstractCard;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class WhiteboardCard extends AbstractCard implements ITitledCard {
    
    VBox content;
    Label titleLabel;
    
    IPin topPin;
    IPin bottomPin;
    IPin leftPin;
    IPin rightPin;

    WhiteboardCard(IDrawingBoard drawing, String title) {
        super(drawing);
        
        titleLabel = new Label( title );
    }

    @Override
    protected Pane createRootUINode() {
        StackPane cardPane = new StackPane();

        cardPane.getChildren().add(getContentNode());

        return cardPane;
    }

    @Override
    protected Node createContentUINode() {
        VBox contentBox = new VBox();

        contentBox.setAlignment(Pos.CENTER);
        contentBox.getChildren().add(this.titleLabel);
        
        // save so we can clear its children on discard()
        content = contentBox;

        return contentBox;
    }

    @Override
    public String getTitle() {
        return titleLabel.getText();
    }

    @Override
    public void setTitle(String text) {
        titleLabel.setText(text);
    }

    @Override
    public void discard() {
        super.discard();

        content.getChildren().clear();
        content = null;
        titleLabel = null;
    }

    @Override
    protected void removePinFromUI( IPin pin ) {
        getRootNode().getChildren().remove( pin.getRootNode() );
    }

    @Override
    protected void addPinToUI(IPin pin) {
        IPin oldPin = null;

        switch(pin.getLocation()) {
        case Top:
            oldPin = topPin;
            topPin = pin;
            break;
        case Bottom:
            oldPin = bottomPin;
            bottomPin = pin;
            break;
        case Left:
            oldPin = leftPin;
            leftPin = pin;
            break;
        case Right:
            oldPin = rightPin;
            rightPin = pin;
            break;
        case Center:
            throw new UnsupportedOperationException("Cant't add pin to card center.");
        }

        if(oldPin != null) {
            throw new UnsupportedOperationException("Can't add pin to already populated location");
        }

        setPinLocation(pin);

        getRootNode().getChildren().add( pin.getRootNode() );
    }

    private boolean setPinLocation(IPin pin) {
        switch (pin.getLocation()) {
        case Top:
            StackPane.setAlignment(pin.getRootNode(), Pos.TOP_CENTER);
            break;
        case Right:
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_RIGHT);
            break;
        case Bottom:
            StackPane.setAlignment(pin.getRootNode(), Pos.BOTTOM_CENTER);
            break;
        case Left:
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_LEFT);
            break;
        case Center:
            throw new UnsupportedOperationException("Cant't set pin location to card center.");
        }
        
        return true;
    }

}
