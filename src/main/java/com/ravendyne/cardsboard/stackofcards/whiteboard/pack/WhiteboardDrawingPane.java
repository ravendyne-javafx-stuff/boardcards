package com.ravendyne.cardsboard.stackofcards.whiteboard.pack;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.samples.SampleDrawingPane;

public class WhiteboardDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return WhiteboardCreator.getInstance();
    }
}
