/**
 * Implementation that outlines basic concepts behind the toolkit.
 * <p>
 * UI is designed to showcase specific pieces and concepts of the toolkit: 
 * cards, pins, connectors, base UI parts, pin connector attachment points etc.
 * </p>
 */
package com.ravendyne.cardsboard.stackofcards.whiteboard;