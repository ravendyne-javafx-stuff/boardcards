package com.ravendyne.cardsboard.stackofcards.whiteboard.pack.card;

import java.util.Objects;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;

public class WhiteboardCardBuilder {
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    String pText;

    public WhiteboardCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public WhiteboardCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

    public WhiteboardCardBuilder setText(String text) {
        pText = text;
        return this;
    }

    public ITitledCard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ITitledCard card = new WhiteboardCard( pDrawing, pTitle );
        card.buildUI();
        pDrawing.addCard(card);

        return card;
    }

}
