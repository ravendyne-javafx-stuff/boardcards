package com.ravendyne.cardsboard.stackofcards.whiteboard.pack.pin;


import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.drawing.AbstractPin;

import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;

public class WhiteboardPin extends AbstractPin {

    public static final double connectorSize = 12;
    
    private double hotspotXParam = 0.5;
    private double hotspotYParam = 0.5;
    
    WhiteboardPin(ICard card) {
        super( card );
    }

    @Override
    protected Region createRootUINode() {
        HBox pinUI = new HBox();
//        pinUI.getStyleClass().add( "debug-region" );

        pinUI.setAlignment(Pos.CENTER);
        
        pinUI.getChildren().add(getConnectorAttachmentNode());
        
        // we have to set width/height on pin in this case,
        // otherwise pin's HBox root container will get auto-stretched
        // to the card width/height and Circle we use for connector
        // attachment point UI will not be rendered in its intended
        // place
        pinUI.setMaxSize(connectorSize, connectorSize);
        
        return pinUI;
    }
    
    @Override
    protected Node createConnectionPoinUINode() {
        return new Rectangle(connectorSize, connectorSize);
    }

    @Override
    public void setLocation(Location location) {
        super.setLocation(location);

        switch(getLocation()) {
        case Top:
            this.getConnectorAttachmentNode().setTranslateY( -10 );
            break;
        case Bottom:
            this.getConnectorAttachmentNode().setTranslateY( +10 );
            break;
        case Left:
            this.getConnectorAttachmentNode().setTranslateX( -10 );
            break;
        case Right:
//            this.getConnectorAttachmentNode().setTranslateX( +10 );
            break;
        case Center:
            throw new IllegalArgumentException("Can't add pin to card center");
        }
    }

    @Override
    public Point2D getAttachmentPositionLocal() {
//        return Point2D.ZERO;
        return new Point2D(hotspotXParam * getConnectorAttachmentNode().getLayoutBounds().getWidth(), hotspotYParam * getConnectorAttachmentNode().getLayoutBounds().getHeight());
    }
}
