package com.ravendyne.cardsboard.stackofcards;

import java.util.List;

import com.ravendyne.dynamofx.util.BasicResourceLoader;

public final class StackOfCardsResources extends BasicResourceLoader {
    public static final StackOfCardsResources loader = new StackOfCardsResources();
    
    public List<String> loadConfig(Class<?> clazz, String configName) {
        List<String> guiConfig = StackOfCardsResources.loader.loadTextFile(StackOfCardsResources.loader.getPackagePath(clazz) + configName);
        return guiConfig;
    }
}
