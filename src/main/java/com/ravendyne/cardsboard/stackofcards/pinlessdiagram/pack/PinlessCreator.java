package com.ravendyne.cardsboard.stackofcards.pinlessdiagram.pack;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder.ConnectorType;
import com.ravendyne.cardsboard.stackofcards.pinlessdiagram.pack.card.PinlessCardBuilder;
import com.ravendyne.cardsboard.stackofcards.pinlessdiagram.pack.pin.PinlessPinBuilder;

public class PinlessCreator implements IDefaultsBuilder {
    private static PinlessCreator instance;
    
    private static ConnectorType defaultConnectorType = ConnectorType.Cubic;

    private PinlessCreator() {}
    
    public static PinlessCreator getInstance() {
        if(instance == null) {
            instance = new PinlessCreator();
        }
        
        return instance;
    }

    public PinlessPinBuilder pin() {
        return new PinlessPinBuilder();
    }

    public PinlessCardBuilder card() {
        return new PinlessCardBuilder();
    }

    public ConnectorBuilder connector() {
        return new ConnectorBuilder();
    }

    public void setDefaultConnectorType(ConnectorType type) {
        defaultConnectorType = type;
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        ICard card = card().setDrawing(drawing).setTitle("'pinless' card").setText("new card").build();

        pin().setCard(card).setLocation(Location.Left).build();
        pin().setCard(card).setLocation(Location.Right).build();
        pin().setCard(card).setLocation(Location.Top).build();
        pin().setCard(card).setLocation(Location.Bottom).build();
        
        return card;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        IConnector connector = connector()
                .setDrawingBoard(drawing)
                .setSource(source)
                .setTarget(target)
                .setType(defaultConnectorType)
                .build();
        return connector;
    }

}
