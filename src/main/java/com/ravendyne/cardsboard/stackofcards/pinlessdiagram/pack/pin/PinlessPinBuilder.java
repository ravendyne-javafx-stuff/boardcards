package com.ravendyne.cardsboard.stackofcards.pinlessdiagram.pack.pin;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

import javafx.scene.paint.Color;

public class PinlessPinBuilder {

    ICard pCard;
    Color pColor;
    Location pLocation;
    
    public PinlessPinBuilder() {
        pColor = Color.web("#00a8f4");
    }
    
    public PinlessPinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public PinlessPinBuilder setColor(Color color) {
        Objects.requireNonNull(color);
        pColor = color;
        return this;
    }

    public PinlessPinBuilder setColor(String colorString) {
        pColor = Color.web(colorString);
        return this;
    }

    public PinlessPinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(pCard);
        Objects.requireNonNull(pColor);
        Objects.requireNonNull(pLocation);

        IPin pin = new TransparentPin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }


}
