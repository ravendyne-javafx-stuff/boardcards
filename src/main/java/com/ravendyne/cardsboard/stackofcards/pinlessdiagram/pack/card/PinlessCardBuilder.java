package com.ravendyne.cardsboard.stackofcards.pinlessdiagram.pack.card;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;

public class PinlessCardBuilder {
    private static final boolean bDebug = false;
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    String pText;

    public PinlessCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public PinlessCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

    public PinlessCardBuilder setText(String text) {
        pText = text;
        return this;
    }

    public ICard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ICard card = new SimpleSquareCard( pDrawing, pTitle );
        card.buildUI();
        pDrawing.addCard(card);
        if(bDebug) System.out.println("added card #" + card.getCardId());

        return card;
    }

}
