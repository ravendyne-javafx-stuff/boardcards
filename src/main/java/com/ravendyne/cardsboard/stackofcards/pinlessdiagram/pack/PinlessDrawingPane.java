package com.ravendyne.cardsboard.stackofcards.pinlessdiagram.pack;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.samples.SampleDrawingPane;

public class PinlessDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return PinlessCreator.getInstance();
    }
}
