package com.ravendyne.cardsboard.stackofcards.circles;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.managers.CardSelectionManager;
import com.ravendyne.cardsboard.stackofcards.StackOfCardsResources;
import com.ravendyne.cardsboard.stackofcards.circles.deck.CirclesDrawingPane;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.util.SandboxApp;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;

public class CirclesApp extends SandboxApp {
    
    // TOOLBAR
    @Inject Button btnClearBoard;
    @Inject Button btnNewCard;
    @Inject Button btnDeleteCard;
    @Inject Button btnDeleteConnector;
    
    // CENTER
    @Inject ScrollPane drawingScrollPane;

    // The good stuff
    IDrawingBoard drawing;

    public CirclesApp() {
        title = "Stack Of Cards: Circles Graph";
        configName = StackOfCardsResources.loader.getPackagePath(getClass()) + "circles.dynamofx";
        styleSheets.add(Styles.getDefaultCss());
        styleSheets.add(CirclesApp.class.getResource("app.css").toExternalForm());
        styleSheets.add(StackOfCardsResources.class.getResource("common.css").toExternalForm());
    }

    @Override
    protected void run() {
        buttonsSetup();

        drawingSetup();
    }

    private void drawingSetup() {
        drawing = new CirclesDrawingPane();
        drawingScrollPane.setContent(drawing.getNode());
    }

    private void onClearBoard() {
        CardSelectionManager.getInstance(drawing).deselectAll();
        drawing.clean();
    }

    private void onNewCard() {
        ICard newCard = drawing.getDefaultsBuilder().newDefaultCard(drawing);
        CardSelectionManager.getInstance(drawing).setSelectedCard(newCard);

        
        // at this point, in order to get any meaningful width/height 
        // of the newly created card, we have to trigger a layout pass on drawing.
        // For a correct answer we have to call applyCss() and layout(), in that order.
        // applyCss() will only work if the card is a child of a Scene, which at this point it
        // is, since it has been added to the drawing.
        drawing.getNode().applyCss();
        drawing.getNode().layout();
        Node node = newCard.getRootNode();
        Bounds cardLayoutBounds = node.getLayoutBounds();
        double cardX = (drawing.getDrawingWidth() - cardLayoutBounds.getWidth()) * Math.random();
        double cardY = (drawing.getDrawingHeight() - cardLayoutBounds.getHeight()) * Math.random();
        newCard.setPosition(new Point2D(cardX, cardY));
    }

    private void onDeleteCard() {
        ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
        if (card != null) {
            drawing.removeCard(card);
            CardSelectionManager.getInstance(drawing).setSelectedCard(null);
        }
    }

    private void onDeleteConnector() {
        IConnector connector = CardSelectionManager.getInstance(drawing).getSelectedConnector();
        if(connector != null) {
            drawing.removeConnector(connector);
            CardSelectionManager.getInstance(drawing).setSelectedConnector(null);
        }
    }

    private void buttonsSetup() {
        Objects.requireNonNull(btnClearBoard, "btnClearBoard not injected");
        Objects.requireNonNull(btnNewCard, "btnNewCard not injected");
        Objects.requireNonNull(btnDeleteCard, "btnDeleteCard not injected");
        Objects.requireNonNull(btnDeleteConnector, "btnDeleteConnector not injected");
        
        Objects.requireNonNull(drawingScrollPane, "drawingScrollPane not injected");

        btnClearBoard.setOnMouseClicked((e) -> onClearBoard());
        btnNewCard.setOnMouseClicked((e) -> onNewCard());
        btnDeleteCard.setOnMouseClicked((e) -> onDeleteCard());
        btnDeleteConnector.setOnMouseClicked((e) -> onDeleteConnector());
    }

    public static void main(String[] args) {
        launch(args);
    }

}
