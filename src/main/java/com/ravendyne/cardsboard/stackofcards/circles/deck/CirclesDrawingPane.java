package com.ravendyne.cardsboard.stackofcards.circles.deck;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.samples.SampleDrawingPane;

public class CirclesDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return CirclesCreator.getInstance();
    }
}
