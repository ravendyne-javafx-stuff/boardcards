package com.ravendyne.cardsboard.stackofcards.circles.deck.pin;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

import javafx.scene.paint.Color;

public class CirclesPinBuilder {

    ICard pCard;
    Color pColor;
    Location pLocation;
    
    public CirclesPinBuilder() {
        pColor = Color.web("#00a8f4");
    }
    
    public CirclesPinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public CirclesPinBuilder setColor(Color color) {
        Objects.requireNonNull(color);
        pColor = color;
        return this;
    }

    public CirclesPinBuilder setColor(String colorString) {
        pColor = Color.web(colorString);
        return this;
    }

    public CirclesPinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(pCard);
        Objects.requireNonNull(pColor);
        Objects.requireNonNull(pLocation);

        IPin pin = new CirclesTransparentPin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }


}
