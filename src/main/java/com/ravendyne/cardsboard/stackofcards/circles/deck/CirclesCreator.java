package com.ravendyne.cardsboard.stackofcards.circles.deck;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsboard.stackofcards.circles.deck.card.CirclesCardBuilder;
import com.ravendyne.cardsboard.stackofcards.circles.deck.connector.CirclesConnectorBuilder;
import com.ravendyne.cardsboard.stackofcards.circles.deck.pin.CirclesPinBuilder;

public class CirclesCreator implements IDefaultsBuilder {
    private static CirclesCreator instance;

    private CirclesCreator() {}
    
    public static CirclesCreator getInstance() {
        if(instance == null) {
            instance = new CirclesCreator();
        }
        
        return instance;
    }

    public CirclesPinBuilder pin() {
        return new CirclesPinBuilder();
    }

    public CirclesCardBuilder card() {
        return new CirclesCardBuilder();
    }

    public CirclesConnectorBuilder connector() {
        return new CirclesConnectorBuilder();
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        ITitledCard card = card()
                .setDrawing(drawing)
                .setTitle("")
                .build();
        card.setTitle("#" + card.getCardId());

        pin().setCard(card).setLocation(Location.Center).build();
        
        return card;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        IConnector connector = connector()
                .setDrawingBoard(drawing)
                .setSource(source)
                .setTarget(target)
                .build();
        return connector;
    }

}
