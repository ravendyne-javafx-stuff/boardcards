package com.ravendyne.cardsboard.stackofcards.circles.deck.connector;

import java.util.Objects;

import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ILabeledConnector;
import com.ravendyne.cardsfx.api.IPin;

public class CirclesConnectorBuilder {
    private static final boolean bDebug = false;

    private IDrawingBoard pDrawing;
    private IPin pSource;
    private IPin pTarget;

    public CirclesConnectorBuilder setDrawingBoard(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public CirclesConnectorBuilder setSource(IPin sourcePin) {
        Objects.requireNonNull(sourcePin);
        pSource = sourcePin;
        return this;
    }

    public CirclesConnectorBuilder setTarget(IPin targetPin) {
        Objects.requireNonNull(targetPin);
        pTarget = targetPin;
        return this;
    }

    public IConnector build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pSource);
        Objects.requireNonNull(pTarget);

        ILabeledConnector connector = new LabeledLineConnectorWithArrow(pDrawing, pSource, pTarget);

        connector.buildUI();
        connector.setLabel("le connector");
        pDrawing.addConnector(connector);
        if(bDebug) System.out.println("added connector from #" + connector.getSource().getPinId() + " to #" + connector.getTarget().getPinId());

        return connector;
    }

}
