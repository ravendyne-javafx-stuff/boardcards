package com.ravendyne.cardsboard.stackofcards.circles.deck.connector;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ILabeledConnector;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.drawing.AbstractConnector;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class LabeledLineConnectorWithArrow extends AbstractConnector implements ILabeledConnector {
    
    private Label meLabel;
    
    private Line meLine;
    Polygon arrowHead;

    LabeledLineConnectorWithArrow(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        super(drawing, sourcePin, targetPin);
        
        meLabel = new Label("");
    }
    
    @Override
    protected Node createRootUINode() {
        Group connectorGroup = new Group();

        arrowHead = new Polygon();
        arrowHead.getPoints().addAll(new Double[] {0.0, 0.0, 10.0, 0.0, 5.0, 20.0});
        arrowHead.getStyleClass().add(Styles.CONNECTOR_ARROW_HEAD_STYLE);
        arrowHead.setStrokeType(StrokeType.INSIDE);
        // this is not needed when using stroke type INSIDE, but we leave it here for illustration
        arrowHead.setStrokeLineJoin(StrokeLineJoin.ROUND);

        arrowLength = 20.0;
        
        arrowHeadRotation = new Rotate();
        // pivot point is ALWAYS relative to shape local coordinate space without any transform applied
        // i.e. doing translate(-5, -20) on arrowHead (which positions its tip to point(0, 5))
        // and then rotate around pivot (0, 5) will NOT rotate the arrow head around its tip.
        arrowHeadRotation.setPivotX(5.0);
        arrowHeadRotation.setPivotY(0.0);
        
        arrowHead.getTransforms().add(new Translate(-5.0, 0.0));
        arrowHead.getTransforms().add(arrowHeadRotation);
        
        connectorGroup.getChildren().add(arrowHead);
        connectorGroup.getChildren().add(meLine);
        connectorGroup.getChildren().add(meLabel);

        return connectorGroup;
    }

    double arrowLength;
    Rotate arrowHeadRotation;

    double arrowDeltaX;
    double arrowDeltaY;

    @Override
    protected Node createConnectorShapeUINode() {
        meLine = new Line();

        return meLine;
    }

    @Override
    public String getLabel() {
        return meLabel.getText();
    }

    @Override
    public void setLabel(String label) {
        meLabel.setText(label);
    }

    @Override
    public void updatePosition() {
        Point2D sourceConnectionPointOnDrawing = source.getAttachmentPosition();
        Point2D targetConnectionPointOnDrawing = target.getAttachmentPosition();

        final double startX = sourceConnectionPointOnDrawing.getX();
        final double startY = sourceConnectionPointOnDrawing.getY();
        final double endX = targetConnectionPointOnDrawing.getX();
        final double endY = targetConnectionPointOnDrawing.getY();
        
        double deltaX = endX - startX;
        double deltaY = endY - startY;
        
        double theta = Math.atan2(deltaY, deltaX);
        arrowDeltaX = arrowLength * Math.cos(theta);
        arrowDeltaY = arrowLength * Math.sin(theta);

        arrowHeadRotation.setAngle(Math.toDegrees(theta) - 90.0);

        meLine.setStartX(startX);
        meLine.setStartY(startY);
        meLine.setEndX(endX - arrowDeltaX);
        meLine.setEndY(endY - arrowDeltaY);
        
        meLabel.relocate((startX + endX - meLabel.getLayoutBounds().getWidth()) / 2, (startY + endY) / 2);

        arrowHead.relocate(endX - arrowDeltaX, endY - arrowDeltaY);
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[source= " + source.getPinId() + ", target=" + target.getPinId() + "]";
    }
}
