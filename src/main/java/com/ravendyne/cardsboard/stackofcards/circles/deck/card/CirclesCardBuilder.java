package com.ravendyne.cardsboard.stackofcards.circles.deck.card;

import java.util.Objects;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;

public class CirclesCardBuilder {
    private static final boolean bDebug = false;
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    
    public CirclesCardBuilder() {
    }

    public CirclesCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public CirclesCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

    public ITitledCard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ITitledCard card = null;
            card = new LittleCircleCard( pDrawing, pTitle );
        card.buildUI();
        pDrawing.addCard(card);
        if(bDebug) System.out.println("added card #" + card.getCardId());

        return card;
    }

}
