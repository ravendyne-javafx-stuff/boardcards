graph [
  node [
    x 631
    cardType "Rectangle"
    y 301
    id 66
    label "DrawingBoardUI"
    type "card"
  ]
  node [
    parent 66
    id 68
    position "Bottom"
    type "pin"
  ]
  node [
    parent 66
    id 70
    position "Left"
    type "pin"
  ]
  node [
    parent 66
    id 69
    position "Right"
    type "pin"
  ]
  node [
    parent 66
    id 67
    position "Top"
    type "pin"
  ]
  node [
    x 53
    cardType "Rectangle"
    y 217
    id 4
    label "EditorUI"
    type "card"
  ]
  node [
    parent 4
    id 17
    position "Top"
    type "pin"
  ]
  node [
    parent 4
    id 20
    position "Bottom"
    type "pin"
  ]
  node [
    parent 4
    id 24
    position "Left"
    type "pin"
  ]
  node [
    parent 4
    id 21
    position "Right"
    type "pin"
  ]
  node [
    x 444
    cardType "Rectangle"
    y 373
    id 31
    label "documentsGroup"
    type "card"
  ]
  node [
    parent 31
    id 34
    position "Right"
    type "pin"
  ]
  node [
    parent 31
    id 33
    position "Bottom"
    type "pin"
  ]
  node [
    parent 31
    id 32
    position "Top"
    type "pin"
  ]
  node [
    parent 31
    id 35
    position "Left"
    type "pin"
  ]
  node [
    x 656
    cardType "Rectangle"
    y 399
    id 51
    label "DocumentToolsUI"
    type "card"
  ]
  node [
    parent 51
    id 54
    position "Right"
    type "pin"
  ]
  node [
    parent 51
    id 55
    position "Left"
    type "pin"
  ]
  node [
    parent 51
    id 53
    position "Bottom"
    type "pin"
  ]
  node [
    parent 51
    id 52
    position "Top"
    type "pin"
  ]
  node [
    x 261
    cardType "Rectangle"
    y 47
    id 3
    label "TopToolsPanel"
    type "card"
  ]
  node [
    parent 3
    id 9
    position "Right"
    type "pin"
  ]
  node [
    parent 3
    id 7
    position "Bottom"
    type "pin"
  ]
  node [
    parent 3
    id 25
    position "Top"
    type "pin"
  ]
  node [
    parent 3
    id 11
    position "Left"
    type "pin"
  ]
  node [
    x 430
    cardType "Rectangle"
    y 143
    id 26
    label "IDrawingBoard"
    type "card"
  ]
  node [
    parent 26
    id 29
    position "Right"
    type "pin"
  ]
  node [
    parent 26
    id 30
    position "Left"
    type "pin"
  ]
  node [
    parent 26
    id 27
    position "Top"
    type "pin"
  ]
  node [
    parent 26
    id 28
    position "Bottom"
    type "pin"
  ]
  node [
    x 448
    cardType "Rectangle"
    y 254
    id 36
    label "drawingGroup"
    type "card"
  ]
  node [
    parent 36
    id 38
    position "Bottom"
    type "pin"
  ]
  node [
    parent 36
    id 40
    position "Left"
    type "pin"
  ]
  node [
    parent 36
    id 37
    position "Top"
    type "pin"
  ]
  node [
    parent 36
    id 39
    position "Right"
    type "pin"
  ]
  node [
    x 465
    cardType "Rectangle"
    y 457
    id 41
    label "MiscBoxUI"
    type "card"
  ]
  node [
    parent 41
    id 43
    position "Bottom"
    type "pin"
  ]
  node [
    parent 41
    id 42
    position "Top"
    type "pin"
  ]
  node [
    parent 41
    id 44
    position "Right"
    type "pin"
  ]
  node [
    parent 41
    id 45
    position "Left"
    type "pin"
  ]
  node [
    x 641
    cardType "Rectangle"
    y 238
    id 61
    label "CardToolsUI"
    type "card"
  ]
  node [
    parent 61
    id 62
    position "Top"
    type "pin"
  ]
  node [
    parent 61
    id 65
    position "Left"
    type "pin"
  ]
  node [
    parent 61
    id 64
    position "Right"
    type "pin"
  ]
  node [
    parent 61
    id 63
    position "Bottom"
    type "pin"
  ]
  node [
    x 253
    cardType "Rectangle"
    y 506
    id 5
    label "RightToolsPanel"
    type "card"
  ]
  node [
    parent 5
    id 18
    position "Right"
    type "pin"
  ]
  node [
    parent 5
    id 13
    position "Top"
    type "pin"
  ]
  node [
    parent 5
    id 22
    position "Left"
    type "pin"
  ]
  node [
    parent 5
    id 15
    position "Bottom"
    type "pin"
  ]
  node [
    x 262
    cardType "Rectangle"
    y 153
    id 1
    label "CenterPanel"
    type "card"
  ]
  node [
    parent 1
    id 16
    position "Bottom"
    type "pin"
  ]
  node [
    parent 1
    id 19
    position "Right"
    type "pin"
  ]
  node [
    parent 1
    id 23
    position "Left"
    type "pin"
  ]
  node [
    parent 1
    id 14
    position "Top"
    type "pin"
  ]
  node [
    x 264
    cardType "Rectangle"
    y 288
    id 2
    label "RightPanel"
    type "card"
  ]
  node [
    parent 2
    id 8
    position "Bottom"
    type "pin"
  ]
  node [
    parent 2
    id 12
    position "Left"
    type "pin"
  ]
  node [
    parent 2
    id 6
    position "Top"
    type "pin"
  ]
  node [
    parent 2
    id 10
    position "Right"
    type "pin"
  ]
  node [
    x 460
    cardType "Rectangle"
    y 535
    id 46
    label "PanelSwitcherUI"
    type "card"
  ]
  node [
    parent 46
    id 47
    position "Top"
    type "pin"
  ]
  node [
    parent 46
    id 48
    position "Bottom"
    type "pin"
  ]
  node [
    parent 46
    id 49
    position "Right"
    type "pin"
  ]
  node [
    parent 46
    id 50
    position "Left"
    type "pin"
  ]
  node [
    x 640
    cardType "Rectangle"
    y 176
    id 56
    label "CardEditUI"
    type "card"
  ]
  node [
    parent 56
    id 57
    position "Top"
    type "pin"
  ]
  node [
    parent 56
    id 60
    position "Left"
    type "pin"
  ]
  node [
    parent 56
    id 58
    position "Bottom"
    type "pin"
  ]
  node [
    parent 56
    id 59
    position "Right"
    type "pin"
  ]
  edge [
    source 19
    type "LineArrow"
    target 30
  ]
  edge [
    source 21
    type "CubicArrow"
    target 23
  ]
  edge [
    source 39
    type "CubicArrow"
    target 60
  ]
  edge [
    source 10
    type "CubicArrow"
    target 40
  ]
  edge [
    source 39
    type "CubicArrow"
    target 65
  ]
  edge [
    source 21
    type "CubicArrow"
    target 12
  ]
  edge [
    source 21
    type "CubicArrow"
    target 22
  ]
  edge [
    source 18
    type "CubicArrow"
    target 50
  ]
  edge [
    source 18
    type "CubicArrow"
    target 45
  ]
  edge [
    source 21
    type "CubicArrow"
    target 11
  ]
  edge [
    source 39
    type "CubicArrow"
    target 70
  ]
  edge [
    source 10
    type "CubicArrow"
    target 35
  ]
  edge [
    source 34
    type "CubicArrow"
    target 55
  ]
  width 1050
  height 630
]
