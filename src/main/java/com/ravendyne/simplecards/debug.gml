graph [
  node [
    x 458
    cardType "Rectangle"
    y 214
    id 3
    label "two"
    type "card"
  ]
  node [
    parent 3
    id 8
    position "Left"
    type "pin"
  ]
  node [
    parent 3
    id 6
    position "Bottom"
    type "pin"
  ]
  node [
    parent 3
    id 5
    position "Top"
    type "pin"
  ]
  node [
    parent 3
    id 7
    position "Right"
    type "pin"
  ]
  node [
    x 176
    cardType "Rectangle"
    y 96
    id 2
    label "one"
    type "card"
  ]
  node [
    parent 2
    id 10
    position "Top"
    type "pin"
  ]
  node [
    parent 2
    id 4
    position "Left"
    type "pin"
  ]
  node [
    parent 2
    id 15
    position "Right"
    type "pin"
  ]
  node [
    parent 2
    id 13
    position "Bottom"
    type "pin"
  ]
  node [
    x 132
    cardType "Text"
    y 320
    id 16
    label "#16"
    text "Some text goes
here for text card"
    type "card"
  ]
  node [
    parent 16
    id 20
    position "Left"
    type "pin"
  ]
  node [
    parent 16
    id 19
    position "Right"
    type "pin"
  ]
  node [
    parent 16
    id 17
    position "Top"
    type "pin"
  ]
  node [
    parent 16
    id 18
    position "Bottom"
    type "pin"
  ]
  edge [
    source 15
    type "CubicArrow"
    target 8
  ]
  width 1050
  height 630
]
