graph [
  node [
    x 192
    y 147
    id 1999
    label "File writer"
    text ""
    type "card"
  ]
  node [
    parent 1999
    id 2000
    position "left"
    type "pin"
  ]
  node [
    parent 1999
    id 2002
    position "top"
    type "pin"
  ]
  node [
    parent 1999
    id 2005
    position "left"
    type "pin"
  ]
  node [
    parent 1999
    id 2007
    position "right"
    type "pin"
  ]
  node [
    parent 1999
    id 2004
    position "bottom"
    type "pin"
  ]
  node [
    parent 1999
    id 2003
    position "bottom"
    type "pin"
  ]
  node [
    parent 1999
    id 2006
    position "top"
    type "pin"
  ]
  node [
    parent 1999
    id 2001
    position "right"
    type "pin"
  ]
  node [
    x 696
    y 698
    id 2116
    label "GML"
    text "- file"
    type "card"
  ]
  node [
    parent 2116
    id 2123
    position "top"
    type "pin"
  ]
  node [
    parent 2116
    id 2122
    position "bottom"
    type "pin"
  ]
  node [
    parent 2116
    id 2118
    position "right"
    type "pin"
  ]
  node [
    parent 2116
    id 2120
    position "bottom"
    type "pin"
  ]
  node [
    parent 2116
    id 2124
    position "right"
    type "pin"
  ]
  node [
    parent 2116
    id 2117
    position "left"
    type "pin"
  ]
  node [
    parent 2116
    id 2119
    position "top"
    type "pin"
  ]
  node [
    parent 2116
    id 2121
    position "left"
    type "pin"
  ]
  node [
    x 19
    y 726
    id 1981
    label "new card"
    text "- write uC code & test with the rig"
    type "card"
  ]
  node [
    parent 1981
    id 1986
    position "right"
    type "pin"
  ]
  node [
    parent 1981
    id 1985
    position "bottom"
    type "pin"
  ]
  node [
    parent 1981
    id 1984
    position "top"
    type "pin"
  ]
  node [
    parent 1981
    id 1987
    position "top"
    type "pin"
  ]
  node [
    parent 1981
    id 1982
    position "left"
    type "pin"
  ]
  node [
    parent 1981
    id 1988
    position "left"
    type "pin"
  ]
  node [
    parent 1981
    id 1989
    position "bottom"
    type "pin"
  ]
  node [
    parent 1981
    id 1983
    position "right"
    type "pin"
  ]
  node [
    x 695
    y 775
    id 2071
    label "dynamo fx"
    text "-"
    type "card"
  ]
  node [
    parent 2071
    id 2074
    position "top"
    type "pin"
  ]
  node [
    parent 2071
    id 2072
    position "left"
    type "pin"
  ]
  node [
    parent 2071
    id 2075
    position "bottom"
    type "pin"
  ]
  node [
    parent 2071
    id 2079
    position "right"
    type "pin"
  ]
  node [
    parent 2071
    id 2073
    position "right"
    type "pin"
  ]
  node [
    parent 2071
    id 2076
    position "left"
    type "pin"
  ]
  node [
    parent 2071
    id 2078
    position "top"
    type "pin"
  ]
  node [
    parent 2071
    id 2077
    position "bottom"
    type "pin"
  ]
  node [
    x 952
    y 155
    id 2098
    label "in C"
    text "- coop engine -> uC
- transpiler c -> Java + sim + dbg
- editor with components & code gen"
    type "card"
  ]
  node [
    parent 2098
    id 2103
    position "bottom"
    type "pin"
  ]
  node [
    parent 2098
    id 2104
    position "left"
    type "pin"
  ]
  node [
    parent 2098
    id 2099
    position "left"
    type "pin"
  ]
  node [
    parent 2098
    id 2102
    position "bottom"
    type "pin"
  ]
  node [
    parent 2098
    id 2106
    position "top"
    type "pin"
  ]
  node [
    parent 2098
    id 2100
    position "right"
    type "pin"
  ]
  node [
    parent 2098
    id 2105
    position "right"
    type "pin"
  ]
  node [
    parent 2098
    id 2101
    position "top"
    type "pin"
  ]
  node [
    x 760
    y 291
    id 1900
    label "byte array impl"
    text ""
    type "card"
  ]
  node [
    parent 1900
    id 1901
    position "left"
    type "pin"
  ]
  node [
    parent 1900
    id 1902
    position "right"
    type "pin"
  ]
  node [
    parent 1900
    id 1905
    position "left"
    type "pin"
  ]
  node [
    parent 1900
    id 1906
    position "bottom"
    type "pin"
  ]
  node [
    parent 1900
    id 1904
    position "bottom"
    type "pin"
  ]
  node [
    parent 1900
    id 1903
    position "top"
    type "pin"
  ]
  node [
    parent 1900
    id 1908
    position "top"
    type "pin"
  ]
  node [
    parent 1900
    id 1907
    position "right"
    type "pin"
  ]
  node [
    x 951
    y 41
    id 1963
    label "in Java"
    text "- dbg view
- POC
- more components
- code generator"
    type "card"
  ]
  node [
    parent 1963
    id 1971
    position "top"
    type "pin"
  ]
  node [
    parent 1963
    id 1965
    position "right"
    type "pin"
  ]
  node [
    parent 1963
    id 1966
    position "top"
    type "pin"
  ]
  node [
    parent 1963
    id 1968
    position "right"
    type "pin"
  ]
  node [
    parent 1963
    id 1970
    position "bottom"
    type "pin"
  ]
  node [
    parent 1963
    id 1964
    position "left"
    type "pin"
  ]
  node [
    parent 1963
    id 1969
    position "left"
    type "pin"
  ]
  node [
    parent 1963
    id 1967
    position "bottom"
    type "pin"
  ]
  node [
    x 1044
    y 312
    id 2008
    label "goals"
    text "- uC prog env
- stepper + drives
- CNC
  - sorter
  - cutter
  - assembler
  - packer"
    type "card"
  ]
  node [
    parent 2008
    id 2013
    position "bottom"
    type "pin"
  ]
  node [
    parent 2008
    id 2012
    position "bottom"
    type "pin"
  ]
  node [
    parent 2008
    id 2016
    position "right"
    type "pin"
  ]
  node [
    parent 2008
    id 2010
    position "right"
    type "pin"
  ]
  node [
    parent 2008
    id 2014
    position "top"
    type "pin"
  ]
  node [
    parent 2008
    id 2009
    position "left"
    type "pin"
  ]
  node [
    parent 2008
    id 2011
    position "top"
    type "pin"
  ]
  node [
    parent 2008
    id 2015
    position "left"
    type "pin"
  ]
  node [
    x 138
    y 285
    id 2089
    label "App"
    text "HTML praser app as FBP network"
    type "card"
  ]
  node [
    parent 2089
    id 2092
    position "top"
    type "pin"
  ]
  node [
    parent 2089
    id 2093
    position "bottom"
    type "pin"
  ]
  node [
    parent 2089
    id 2097
    position "right"
    type "pin"
  ]
  node [
    parent 2089
    id 2091
    position "right"
    type "pin"
  ]
  node [
    parent 2089
    id 2094
    position "left"
    type "pin"
  ]
  node [
    parent 2089
    id 2096
    position "top"
    type "pin"
  ]
  node [
    parent 2089
    id 2095
    position "bottom"
    type "pin"
  ]
  node [
    parent 2089
    id 2090
    position "left"
    type "pin"
  ]
  node [
    x 486
    y 391
    id 1909
    label "with array"
    text ""
    type "card"
  ]
  node [
    parent 1909
    id 1912
    position "top"
    type "pin"
  ]
  node [
    parent 1909
    id 1914
    position "bottom"
    type "pin"
  ]
  node [
    parent 1909
    id 1910
    position "left"
    type "pin"
  ]
  node [
    parent 1909
    id 1911
    position "right"
    type "pin"
  ]
  node [
    parent 1909
    id 1916
    position "top"
    type "pin"
  ]
  node [
    parent 1909
    id 1913
    position "bottom"
    type "pin"
  ]
  node [
    parent 1909
    id 1915
    position "right"
    type "pin"
  ]
  node [
    parent 1909
    id 1917
    position "left"
    type "pin"
  ]
  node [
    x 208
    y 82
    id 2143
    label "File reader"
    text ""
    type "card"
  ]
  node [
    parent 2143
    id 2151
    position "left"
    type "pin"
  ]
  node [
    parent 2143
    id 2144
    position "left"
    type "pin"
  ]
  node [
    parent 2143
    id 2150
    position "right"
    type "pin"
  ]
  node [
    parent 2143
    id 2145
    position "right"
    type "pin"
  ]
  node [
    parent 2143
    id 2146
    position "top"
    type "pin"
  ]
  node [
    parent 2143
    id 2149
    position "top"
    type "pin"
  ]
  node [
    parent 2143
    id 2147
    position "bottom"
    type "pin"
  ]
  node [
    parent 2143
    id 2148
    position "bottom"
    type "pin"
  ]
  node [
    x 223
    y 563
    id 1927
    label "new card"
    text "- create second order (velocity driven)
"
    type "card"
  ]
  node [
    parent 1927
    id 1935
    position "top"
    type "pin"
  ]
  node [
    parent 1927
    id 1930
    position "top"
    type "pin"
  ]
  node [
    parent 1927
    id 1934
    position "right"
    type "pin"
  ]
  node [
    parent 1927
    id 1933
    position "bottom"
    type "pin"
  ]
  node [
    parent 1927
    id 1932
    position "left"
    type "pin"
  ]
  node [
    parent 1927
    id 1928
    position "left"
    type "pin"
  ]
  node [
    parent 1927
    id 1929
    position "right"
    type "pin"
  ]
  node [
    parent 1927
    id 1931
    position "bottom"
    type "pin"
  ]
  node [
    x 720
    y 393
    id 1936
    label "with gpu"
    text ""
    type "card"
  ]
  node [
    parent 1936
    id 1938
    position "right"
    type "pin"
  ]
  node [
    parent 1936
    id 1937
    position "left"
    type "pin"
  ]
  node [
    parent 1936
    id 1941
    position "left"
    type "pin"
  ]
  node [
    parent 1936
    id 1939
    position "top"
    type "pin"
  ]
  node [
    parent 1936
    id 1940
    position "bottom"
    type "pin"
  ]
  node [
    parent 1936
    id 1942
    position "top"
    type "pin"
  ]
  node [
    parent 1936
    id 1943
    position "right"
    type "pin"
  ]
  node [
    parent 1936
    id 1944
    position "bottom"
    type "pin"
  ]
  node [
    x 978
    y 733
    id 2053
    label "file viewer"
    text ""
    type "card"
  ]
  node [
    parent 2053
    id 2054
    position "left"
    type "pin"
  ]
  node [
    parent 2053
    id 2061
    position "left"
    type "pin"
  ]
  node [
    parent 2053
    id 2059
    position "bottom"
    type "pin"
  ]
  node [
    parent 2053
    id 2058
    position "top"
    type "pin"
  ]
  node [
    parent 2053
    id 2060
    position "right"
    type "pin"
  ]
  node [
    parent 2053
    id 2055
    position "right"
    type "pin"
  ]
  node [
    parent 2053
    id 2057
    position "bottom"
    type "pin"
  ]
  node [
    parent 2053
    id 2056
    position "top"
    type "pin"
  ]
  node [
    x 571
    y 202
    id 1891
    label "particle simulation"
    text ""
    type "card"
  ]
  node [
    parent 1891
    id 1892
    position "left"
    type "pin"
  ]
  node [
    parent 1891
    id 1897
    position "right"
    type "pin"
  ]
  node [
    parent 1891
    id 1896
    position "top"
    type "pin"
  ]
  node [
    parent 1891
    id 1899
    position "left"
    type "pin"
  ]
  node [
    parent 1891
    id 1898
    position "bottom"
    type "pin"
  ]
  node [
    parent 1891
    id 1895
    position "bottom"
    type "pin"
  ]
  node [
    parent 1891
    id 1893
    position "right"
    type "pin"
  ]
  node [
    parent 1891
    id 1894
    position "top"
    type "pin"
  ]
  node [
    x 573
    y 140
    id 1990
    label "graph"
    text "-"
    type "card"
  ]
  node [
    parent 1990
    id 1992
    position "right"
    type "pin"
  ]
  node [
    parent 1990
    id 1997
    position "top"
    type "pin"
  ]
  node [
    parent 1990
    id 1998
    position "left"
    type "pin"
  ]
  node [
    parent 1990
    id 1991
    position "left"
    type "pin"
  ]
  node [
    parent 1990
    id 1995
    position "right"
    type "pin"
  ]
  node [
    parent 1990
    id 1993
    position "top"
    type "pin"
  ]
  node [
    parent 1990
    id 1996
    position "bottom"
    type "pin"
  ]
  node [
    parent 1990
    id 1994
    position "bottom"
    type "pin"
  ]
  node [
    x 1072
    y 465
    id 2044
    label "sw for above to program them"
    text ""
    type "card"
  ]
  node [
    parent 2044
    id 2045
    position "left"
    type "pin"
  ]
  node [
    parent 2044
    id 2051
    position "top"
    type "pin"
  ]
  node [
    parent 2044
    id 2047
    position "top"
    type "pin"
  ]
  node [
    parent 2044
    id 2052
    position "right"
    type "pin"
  ]
  node [
    parent 2044
    id 2050
    position "bottom"
    type "pin"
  ]
  node [
    parent 2044
    id 2046
    position "right"
    type "pin"
  ]
  node [
    parent 2044
    id 2049
    position "left"
    type "pin"
  ]
  node [
    parent 2044
    id 2048
    position "bottom"
    type "pin"
  ]
  node [
    x 572
    y 14
    id 1972
    label "FBP"
    text "-"
    type "card"
  ]
  node [
    parent 1972
    id 1974
    position "right"
    type "pin"
  ]
  node [
    parent 1972
    id 1973
    position "left"
    type "pin"
  ]
  node [
    parent 1972
    id 1976
    position "bottom"
    type "pin"
  ]
  node [
    parent 1972
    id 1977
    position "right"
    type "pin"
  ]
  node [
    parent 1972
    id 1979
    position "left"
    type "pin"
  ]
  node [
    parent 1972
    id 1980
    position "bottom"
    type "pin"
  ]
  node [
    parent 1972
    id 1978
    position "top"
    type "pin"
  ]
  node [
    parent 1972
    id 1975
    position "top"
    type "pin"
  ]
  node [
    x 517
    y 294
    id 1918
    label "gravity sim"
    text ""
    type "card"
  ]
  node [
    parent 1918
    id 1920
    position "right"
    type "pin"
  ]
  node [
    parent 1918
    id 1922
    position "bottom"
    type "pin"
  ]
  node [
    parent 1918
    id 1924
    position "right"
    type "pin"
  ]
  node [
    parent 1918
    id 1919
    position "left"
    type "pin"
  ]
  node [
    parent 1918
    id 1921
    position "top"
    type "pin"
  ]
  node [
    parent 1918
    id 1923
    position "top"
    type "pin"
  ]
  node [
    parent 1918
    id 1925
    position "bottom"
    type "pin"
  ]
  node [
    parent 1918
    id 1926
    position "left"
    type "pin"
  ]
  node [
    x 698
    y 547
    id 1945
    label "java three"
    text "-"
    type "card"
  ]
  node [
    parent 1945
    id 1947
    position "right"
    type "pin"
  ]
  node [
    parent 1945
    id 1949
    position "bottom"
    type "pin"
  ]
  node [
    parent 1945
    id 1950
    position "right"
    type "pin"
  ]
  node [
    parent 1945
    id 1946
    position "left"
    type "pin"
  ]
  node [
    parent 1945
    id 1952
    position "left"
    type "pin"
  ]
  node [
    parent 1945
    id 1951
    position "top"
    type "pin"
  ]
  node [
    parent 1945
    id 1948
    position "top"
    type "pin"
  ]
  node [
    parent 1945
    id 1953
    position "bottom"
    type "pin"
  ]
  node [
    x 14
    y 19
    id 1954
    label "FBP"
    text "Components"
    type "card"
  ]
  node [
    parent 1954
    id 1957
    position "top"
    type "pin"
  ]
  node [
    parent 1954
    id 1959
    position "top"
    type "pin"
  ]
  node [
    parent 1954
    id 1960
    position "right"
    type "pin"
  ]
  node [
    parent 1954
    id 1955
    position "left"
    type "pin"
  ]
  node [
    parent 1954
    id 1961
    position "left"
    type "pin"
  ]
  node [
    parent 1954
    id 1956
    position "right"
    type "pin"
  ]
  node [
    parent 1954
    id 1958
    position "bottom"
    type "pin"
  ]
  node [
    parent 1954
    id 1962
    position "bottom"
    type "pin"
  ]
  node [
    x 700
    y 617
    id 2125
    label "icecap"
    text "- viewer
- method graph"
    type "card"
  ]
  node [
    parent 2125
    id 2131
    position "right"
    type "pin"
  ]
  node [
    parent 2125
    id 2126
    position "left"
    type "pin"
  ]
  node [
    parent 2125
    id 2130
    position "top"
    type "pin"
  ]
  node [
    parent 2125
    id 2133
    position "left"
    type "pin"
  ]
  node [
    parent 2125
    id 2128
    position "top"
    type "pin"
  ]
  node [
    parent 2125
    id 2132
    position "bottom"
    type "pin"
  ]
  node [
    parent 2125
    id 2127
    position "right"
    type "pin"
  ]
  node [
    parent 2125
    id 2129
    position "bottom"
    type "pin"
  ]
  node [
    x 87
    y 415
    id 2026
    label "bezier stepper driver"
    text "-"
    type "card"
  ]
  node [
    parent 2026
    id 2032
    position "right"
    type "pin"
  ]
  node [
    parent 2026
    id 2027
    position "left"
    type "pin"
  ]
  node [
    parent 2026
    id 2031
    position "bottom"
    type "pin"
  ]
  node [
    parent 2026
    id 2033
    position "left"
    type "pin"
  ]
  node [
    parent 2026
    id 2034
    position "top"
    type "pin"
  ]
  node [
    parent 2026
    id 2029
    position "top"
    type "pin"
  ]
  node [
    parent 2026
    id 2028
    position "right"
    type "pin"
  ]
  node [
    parent 2026
    id 2030
    position "bottom"
    type "pin"
  ]
  node [
    x 1072
    y 527
    id 2134
    label "air plane + launcher"
    text ""
    type "card"
  ]
  node [
    parent 2134
    id 2139
    position "left"
    type "pin"
  ]
  node [
    parent 2134
    id 2136
    position "right"
    type "pin"
  ]
  node [
    parent 2134
    id 2137
    position "top"
    type "pin"
  ]
  node [
    parent 2134
    id 2142
    position "bottom"
    type "pin"
  ]
  node [
    parent 2134
    id 2140
    position "right"
    type "pin"
  ]
  node [
    parent 2134
    id 2138
    position "bottom"
    type "pin"
  ]
  node [
    parent 2134
    id 2141
    position "top"
    type "pin"
  ]
  node [
    parent 2134
    id 2135
    position "left"
    type "pin"
  ]
  node [
    x 178
    y 215
    id 2107
    label "IP shredder"
    text ""
    type "card"
  ]
  node [
    parent 2107
    id 2108
    position "left"
    type "pin"
  ]
  node [
    parent 2107
    id 2111
    position "bottom"
    type "pin"
  ]
  node [
    parent 2107
    id 2114
    position "right"
    type "pin"
  ]
  node [
    parent 2107
    id 2113
    position "top"
    type "pin"
  ]
  node [
    parent 2107
    id 2115
    position "left"
    type "pin"
  ]
  node [
    parent 2107
    id 2109
    position "right"
    type "pin"
  ]
  node [
    parent 2107
    id 2110
    position "top"
    type "pin"
  ]
  node [
    parent 2107
    id 2112
    position "bottom"
    type "pin"
  ]
  node [
    x 201
    y 659
    id 2035
    label "new card"
    text "- test 2nd order"
    type "card"
  ]
  node [
    parent 2035
    id 2041
    position "left"
    type "pin"
  ]
  node [
    parent 2035
    id 2040
    position "bottom"
    type "pin"
  ]
  node [
    parent 2035
    id 2036
    position "left"
    type "pin"
  ]
  node [
    parent 2035
    id 2038
    position "top"
    type "pin"
  ]
  node [
    parent 2035
    id 2039
    position "bottom"
    type "pin"
  ]
  node [
    parent 2035
    id 2043
    position "right"
    type "pin"
  ]
  node [
    parent 2035
    id 2042
    position "top"
    type "pin"
  ]
  node [
    parent 2035
    id 2037
    position "right"
    type "pin"
  ]
  node [
    x 240
    y 491
    id 2062
    label "new card"
    text "- test first order (position driven)"
    type "card"
  ]
  node [
    parent 2062
    id 2064
    position "right"
    type "pin"
  ]
  node [
    parent 2062
    id 2063
    position "left"
    type "pin"
  ]
  node [
    parent 2062
    id 2065
    position "top"
    type "pin"
  ]
  node [
    parent 2062
    id 2069
    position "left"
    type "pin"
  ]
  node [
    parent 2062
    id 2066
    position "bottom"
    type "pin"
  ]
  node [
    parent 2062
    id 2068
    position "bottom"
    type "pin"
  ]
  node [
    parent 2062
    id 2070
    position "top"
    type "pin"
  ]
  node [
    parent 2062
    id 2067
    position "right"
    type "pin"
  ]
  node [
    x 573
    y 75
    id 2080
    label "FX blueprints"
    text "-"
    type "card"
  ]
  node [
    parent 2080
    id 2085
    position "right"
    type "pin"
  ]
  node [
    parent 2080
    id 2084
    position "bottom"
    type "pin"
  ]
  node [
    parent 2080
    id 2086
    position "top"
    type "pin"
  ]
  node [
    parent 2080
    id 2088
    position "bottom"
    type "pin"
  ]
  node [
    parent 2080
    id 2087
    position "left"
    type "pin"
  ]
  node [
    parent 2080
    id 2081
    position "left"
    type "pin"
  ]
  node [
    parent 2080
    id 2083
    position "top"
    type "pin"
  ]
  node [
    parent 2080
    id 2082
    position "right"
    type "pin"
  ]
  node [
    x 978
    y 667
    id 2017
    label "file parser"
    text ""
    type "card"
  ]
  node [
    parent 2017
    id 2018
    position "left"
    type "pin"
  ]
  node [
    parent 2017
    id 2025
    position "left"
    type "pin"
  ]
  node [
    parent 2017
    id 2020
    position "top"
    type "pin"
  ]
  node [
    parent 2017
    id 2023
    position "bottom"
    type "pin"
  ]
  node [
    parent 2017
    id 2024
    position "right"
    type "pin"
  ]
  node [
    parent 2017
    id 2022
    position "top"
    type "pin"
  ]
  node [
    parent 2017
    id 2019
    position "right"
    type "pin"
  ]
  node [
    parent 2017
    id 2021
    position "bottom"
    type "pin"
  ]
  edge [
    label "conn"
    source 1898
    type "cubic"
    target 1923
  ]
  edge [
    label "conn"
    source 1962
    type "cubic"
    target 2005
  ]
  edge [
    label "conn"
    source 2031
    type "cubic"
    target 1932
  ]
  edge [
    label "conn"
    source 1962
    type "cubic"
    target 2094
  ]
  edge [
    label "conn"
    source 1962
    type "cubic"
    target 2151
  ]
  edge [
    label "conn"
    source 2124
    type "cubic"
    target 2025
  ]
  edge [
    label "conn"
    source 1925
    type "cubic"
    target 1942
  ]
  edge [
    label "conn"
    source 2031
    type "cubic"
    target 2069
  ]
  edge [
    label "conn"
    source 2031
    type "cubic"
    target 1987
  ]
  edge [
    label "conn"
    source 1925
    type "cubic"
    target 1916
  ]
  edge [
    label "conn"
    source 2124
    type "cubic"
    target 2061
  ]
  edge [
    label "conn"
    source 1898
    type "cubic"
    target 1908
  ]
  edge [
    label "conn"
    source 1962
    type "cubic"
    target 2115
  ]
  edge [
    label "conn"
    source 2031
    type "cubic"
    target 2041
  ]
  width 1306
  height 856
]
