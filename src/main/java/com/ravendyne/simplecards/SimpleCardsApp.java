package com.ravendyne.simplecards;

import java.util.List;

import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.dynamofx.DynamoFxComponent;
import com.ravendyne.simplecards.files.AppPreferences;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class SimpleCardsApp extends Application {
    private static Stage primaryStage;
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        SimpleCardsApp.primaryStage = primaryStage;
        primaryStage.setTitle("JavaFX Viewer");
        Image icon = SimpleCardsResources.loader.loadFxImage("/icons/dfxicon.png");
        primaryStage.getIcons().add(icon);

        List<String> guiConfig = SimpleCardsResources.loader.loadTextFile("simplecards.dynamofx");
        SimpleCardsBoard cardsBoard = new SimpleCardsBoard();
        Parent node = DynamoFxComponent
            .newInstance("editor", guiConfig)
            .setResourceLoader( SimpleCardsResources.loader )
            .injectInto( cardsBoard )
            .build();

        Scene scene = new Scene(node);

        primaryStage.setScene(scene);

        String windowState = AppPreferences.INSTANCE.getPreference(AppPreferences.WINDOW_STATE_KEY);
        if(windowState != null && windowState.equals("maximized")) {
            primaryStage.setMaximized(true);
        }

        primaryStage.maximizedProperty().addListener((a,b,c) -> {
            String state = c ? "maximized" : "normal";
            AppPreferences.INSTANCE.setPreference(AppPreferences.WINDOW_STATE_KEY, state);
            AppPreferences.INSTANCE.save();
        });

        scene.getStylesheets().add(Styles.getDefaultCss());
        scene.getStylesheets().add(SimpleCardsApp.class.getResource("app.css").toExternalForm());
        scene.getStylesheets().add(SimpleCardsApp.class.getResource("drawing.css").toExternalForm());

        primaryStage.setOnShown( new EventHandler<WindowEvent>() {
            @Override
            public void handle( WindowEvent event ) {
                cardsBoard.run(primaryStage);
            }
        } );

        primaryStage.show();
    }
    
    public static void setTitle(String title) {
        if(primaryStage != null) {
            primaryStage.setTitle(title);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}
