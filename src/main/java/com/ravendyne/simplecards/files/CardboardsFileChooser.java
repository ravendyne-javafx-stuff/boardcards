package com.ravendyne.simplecards.files;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class CardboardsFileChooser implements EventHandler<ActionEvent> {
    
    public static enum Type {
        Open,
        Save
    }

    private final Node ownerNode;
    private final Consumer< String > consumer;
    FileChooser chooser;
    Type type;

    public CardboardsFileChooser( String title, Node ownerNode, Type type, Consumer< String > consumer ) {
        this.type = type;
        this.consumer = consumer;
        this.ownerNode = ownerNode;

        chooser = new FileChooser();
        chooser.setTitle(title);

        File initialFolder = new File(AppPreferences.INSTANCE.getLastUsedFolder());
        if( ! initialFolder.exists() ) {
            initialFolder = new File( Paths.get( "." ).toAbsolutePath().toString() );
        }
        chooser.setInitialDirectory( initialFolder );

        chooser.getExtensionFilters().addAll(
                new ExtensionFilter("Cardsboard Files", "*.gml"),
                new ExtensionFilter("All Files", "*.*"));
    }

    @Override
    public void handle(ActionEvent event) {

        File selectedFile = null;

        switch (type) {
        case Open:
            selectedFile = chooser.showOpenDialog(ownerNode.getScene().getWindow());
            break;

        case Save:
            selectedFile = chooser.showSaveDialog(ownerNode.getScene().getWindow());
            break;
        }

        if (selectedFile != null) {
            final Path selectedPath = Paths.get( selectedFile.getAbsolutePath() ).normalize();
            String chosenFilePath = selectedPath.toString();
            AppPreferences.INSTANCE.setLastUsedFolder(selectedPath.getParent().toString());
            AppPreferences.INSTANCE.save();
            consumer.accept( chosenFilePath );
        }
    }

}
