package com.ravendyne.simplecards.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

public enum AppPreferences {
    INSTANCE;
    
    public static final String SETTINGS_FILE = ".cardsboard";

    public static final String LAST_OPEN_FILE_KEY = "lastFile";
    public static final String LAST_USED_FOLDER_KEY = "lastFolder";
    public static final String WINDOW_STATE_KEY = "windowState";
    
    Properties props;
    File settingsFile;
    
    private AppPreferences() {
        props = new Properties();
        settingsFile = Paths.get( ".", SETTINGS_FILE ).toAbsolutePath().toFile();

        try {
            FileInputStream inStream = new FileInputStream(settingsFile);
            props.load(inStream);
            inStream.close();
        } catch (IOException e) {
            props.clear();
            props.setProperty(LAST_USED_FOLDER_KEY, Paths.get( "." ).toAbsolutePath().toString());
        }
    }
    
    public String getLastUsedFolder() {
        return props.getProperty(LAST_USED_FOLDER_KEY);
    }
    
    public void setLastUsedFolder(String folder) {
        props.setProperty(LAST_USED_FOLDER_KEY, folder);
    }
    
    public String getLastOpenFile() {
        return props.getProperty(LAST_OPEN_FILE_KEY);
    }
    
    public void setLastOpenFile(String filename) {
        props.setProperty(LAST_OPEN_FILE_KEY, filename);
    }
    
    public String getPreference(String name) {
        return props.getProperty(name);
    }
    
    public void setPreference(String name, String value) {
        props.setProperty(name, value);
    }
    
    public void save() {
        FileOutputStream out;
        try {
            out = new FileOutputStream(settingsFile);
            props.store(out, "Cardsboard App Settings. Auto-generated.");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
