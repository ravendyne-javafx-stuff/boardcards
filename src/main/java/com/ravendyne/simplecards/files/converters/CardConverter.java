package com.ravendyne.simplecards.files.converters;

import java.util.Map;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.constellation.graph.generic.gml.Gml;
import com.ravendyne.constellation.graph.generic.gml.GmlBuilder;
import com.ravendyne.constellation.graph.generic.gml.GmlValue;
import com.ravendyne.constellation.graph.generic.gml.GmlValueBuilder;
import com.ravendyne.simplecards.deck.SimpleCardsCreator;
import com.ravendyne.simplecards.deck.card.ICardWithText;
import com.ravendyne.simplecards.deck.card.SCCardBuilder.DocsCardType;
import com.ravendyne.simplecards.files.GraphLoader;

import javafx.geometry.Point2D;

public final class CardConverter {
    private CardConverter() {}
    
    public static ICard fromGml(IDrawingBoard pane, Map<String, GmlValue> attrs) {
        String title = attrs.get(Gml.NODE_LABEL_TAG).getString();
        GmlValue textValue = attrs.get(GraphLoader.GML_NODE_ATTR_TEXT);
        String text = "";
        if(textValue != null) {
            text = textValue.getString();
        }

        Point2D position = new Point2D(Math.random() * pane.getDrawingWidth(), Math.random() * pane.getDrawingHeight());
        double x = position.getX();
        double y = position.getY();

        GmlValue xVal = attrs.get(GraphLoader.GML_NODE_ATTR_X);
        if(xVal != null) {
            x = xVal.getNumber().doubleValue();
        }
        GmlValue yVal = attrs.get(GraphLoader.GML_NODE_ATTR_Y);
        if(yVal != null) {
            y = yVal.getNumber().doubleValue();
        }

        position = new Point2D(x, y);

        GmlValue typeValue = attrs.get(GraphLoader.GML_NODE_ATTR_CARD_TYPE);
        DocsCardType cardType = DocsCardType.Rectangle;
        if(typeValue != null && typeValue.isString()) {
            cardType = ConverterUtils.getEnumValue(DocsCardType.class, typeValue.getString());
        }
        ICard card = SimpleCardsCreator.getInstance().card()
                .setDrawing(pane)
                .setType(cardType)
                .setTitle(title)
                .setText(text)
                .build();

        card.setPosition( position );
        
        return card;
    }
    
    private static void addCardWithTextAttrs(ICardWithText card, GmlValueBuilder cardValue) {
        cardValue.setMapItem(GraphLoader.GML_NODE_ATTR_TEXT, GmlBuilder.newStringValue( card.getText() ));
    }

    private static void setCardTypeAttr(ICard card, GmlValueBuilder cardValue) {
        DocsCardType cardType = SimpleCardsCreator.getInstance().card().getCardType(card);
        cardValue.setMapItem(GraphLoader.GML_NODE_ATTR_CARD_TYPE, GmlBuilder.newStringValue( cardType.name() ));
    }

    public static GmlValue toGml(ITitledCard card) {
        GmlValueBuilder nodeList = GmlBuilder.newListValue();

        GmlValueBuilder cardValue = GmlBuilder.newMapValue();
        cardValue.setMapItem(Gml.NODE_ID_TAG, GmlBuilder.newNumberValue( card.getCardId() ));
        cardValue.setMapItem(GraphLoader.GML_NODE_ATTR_TYPE, GmlBuilder.newStringValue( GraphLoader.GML_NODE_TYPE_CARD ));
        cardValue.setMapItem(Gml.NODE_LABEL_TAG, GmlBuilder.newStringValue( card.getTitle() ));
        Point2D position = card.getPosition();
        cardValue.setMapItem(GraphLoader.GML_NODE_ATTR_X, GmlBuilder.newNumberValue( (int)position.getX() ));
        cardValue.setMapItem(GraphLoader.GML_NODE_ATTR_Y, GmlBuilder.newNumberValue( (int)position.getY() ));

        setCardTypeAttr(card, cardValue);

        if(card instanceof ICardWithText) {
            addCardWithTextAttrs((ICardWithText)card, cardValue);
        }

        nodeList.addListItem(cardValue);

        // pins are nodes for themselves, but with type = "pin"
        for(IPin pin : card.getPins()) {
            GmlValue pinValue = PinConverter.toGml(pin);
            nodeList.addListItem(pinValue);
        }

        return nodeList;
    }

}
