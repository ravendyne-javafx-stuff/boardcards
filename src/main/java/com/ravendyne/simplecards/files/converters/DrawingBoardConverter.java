package com.ravendyne.simplecards.files.converters;

import java.util.Map;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.constellation.graph.generic.gml.GmlBuilder;
import com.ravendyne.constellation.graph.generic.gml.GmlValue;
import com.ravendyne.constellation.graph.generic.gml.GmlValueBuilder;
import com.ravendyne.simplecards.files.GraphLoader;

public final class DrawingBoardConverter {
    private DrawingBoardConverter() {}

    public static GmlValueBuilder toGml(IDrawingBoard drawing) {
        GmlValueBuilder drawingMap = GmlBuilder.newMapValue();

        drawingMap.setMapItem(GraphLoader.GML_GRAPH_ATTR_WIDTH, GmlBuilder.newNumberValue( (int)drawing.getDrawingWidth() ));
        drawingMap.setMapItem(GraphLoader.GML_GRAPH_ATTR_HEIGHT, GmlBuilder.newNumberValue( (int)drawing.getDrawingHeight() ));
        
        return drawingMap;
    }
    
    public static void updateFromGml(IDrawingBoard drawing, Map<String, GmlValue> attrs) {
        double width = attrs.get(GraphLoader.GML_GRAPH_ATTR_WIDTH).getNumber().doubleValue();
        double height = attrs.get(GraphLoader.GML_GRAPH_ATTR_HEIGHT).getNumber().doubleValue();

        drawing.setDrawingWidth(width);
        drawing.setDrawingHeight(height);
    }
}
