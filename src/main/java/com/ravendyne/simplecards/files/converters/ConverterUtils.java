package com.ravendyne.simplecards.files.converters;


public final class ConverterUtils {
    private ConverterUtils() {}

    public static <T extends Enum<T>> T getEnumValue(Class<T> enumType, String value) {
        T result = null;
        try {
            result = Enum.valueOf(enumType, value);
        } catch (IllegalArgumentException ex) {
            System.err.println("Value '" + value +"' doesn't exist in '" + enumType.getName() + "'" );
        }
        return result;
    }

}
