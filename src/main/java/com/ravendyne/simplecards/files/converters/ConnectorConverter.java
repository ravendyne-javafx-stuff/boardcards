package com.ravendyne.simplecards.files.converters;

import java.util.Map;

import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.constellation.graph.generic.gml.Gml;
import com.ravendyne.constellation.graph.generic.gml.GmlBuilder;
import com.ravendyne.constellation.graph.generic.gml.GmlValue;
import com.ravendyne.constellation.graph.generic.gml.GmlValueBuilder;
import com.ravendyne.simplecards.deck.SimpleCardsCreator;
import com.ravendyne.simplecards.deck.connector.BezierConnector;
import com.ravendyne.simplecards.deck.connector.BezierConnectorWithArrow;
import com.ravendyne.simplecards.deck.connector.LineConnector;
import com.ravendyne.simplecards.deck.connector.LineConnectorWithArrow;
import com.ravendyne.simplecards.deck.connector.SCConnectorBuilder;
import com.ravendyne.simplecards.deck.connector.SCConnectorBuilder.ConnectorType;
import com.ravendyne.simplecards.files.GraphLoader;

public final class ConnectorConverter {
    private ConnectorConverter() {}
    
    public static IConnector fromGml(IDrawingBoard drawing, IPin sourcePin, IPin targetPin, Map<String, GmlValue> attrs) {
        final SCConnectorBuilder builder = SimpleCardsCreator.getInstance().connector()
                .setDrawingBoard(drawing)
                .setSource(sourcePin)
                .setTarget(targetPin);

        GmlValue typeValue = attrs.get( GraphLoader.GML_EDGE_ATTR_TYPE );
        if(typeValue != null) {
            String typeString = typeValue.getString();
            ConnectorType type = ConverterUtils.getEnumValue(ConnectorType.class, typeString);
            builder.setType(type);
        }

        IConnector connector = builder.build();
        
        return connector;
    }

    public static GmlValue toGml(IConnector connector) {
        GmlValueBuilder edge = GmlBuilder.newMapValue();

        edge.setMapItem(Gml.EDGE_SOURCE_TAG, GmlBuilder.newNumberValue(connector.getSource().getPinId()));
        edge.setMapItem(Gml.EDGE_TARGET_TAG, GmlBuilder.newNumberValue(connector.getTarget().getPinId()));
        String type = ConnectorType.Line.name();
        if(connector instanceof LineConnector) {
            type = ConnectorType.Line.name();
        } else if(connector instanceof LineConnectorWithArrow) {
            type = ConnectorType.LineArrow.name();
        } else if(connector instanceof BezierConnector) {
            type = ConnectorType.Cubic.name();
        } else if(connector instanceof BezierConnectorWithArrow) {
            type = ConnectorType.CubicArrow.name();
        }
        edge.setMapItem(GraphLoader.GML_EDGE_ATTR_TYPE, GmlBuilder.newStringValue(type));

        return edge;
    }

}
