package com.ravendyne.simplecards.files.converters;

import java.util.Map;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.constellation.graph.generic.gml.Gml;
import com.ravendyne.constellation.graph.generic.gml.GmlBuilder;
import com.ravendyne.constellation.graph.generic.gml.GmlValue;
import com.ravendyne.constellation.graph.generic.gml.GmlValueBuilder;
import com.ravendyne.simplecards.deck.SimpleCardsCreator;
import com.ravendyne.simplecards.files.GraphLoader;

import javafx.scene.paint.Color;

public final class PinConverter {
    private PinConverter() {}

    public static IPin fromGml(ICard card, Map<String, GmlValue> attrs) {
        String colorString = "#00a8f4";
        GmlValue colorStringValue = attrs.get( GraphLoader.GML_NODE_ATTR_COLOR );
        if(colorStringValue != null) {
            colorString = colorStringValue.getString();
        }

        String position = attrs.get(GraphLoader.GML_NODE_ATTR_POSITION).getString();
        Location location = ConverterUtils.getEnumValue(Location.class, position);
        
        IPin pin = SimpleCardsCreator.getInstance().pin()
            .setCard(card)
            .setColor(Color.web(colorString))
            .setLocation(location)
            .build();
        
        return pin;
    }

    public static GmlValue toGml(IPin pin) {
        GmlValueBuilder pinValue = GmlBuilder.newMapValue();
        pinValue.setMapItem(Gml.NODE_ID_TAG, GmlBuilder.newNumberValue( pin.getPinId() ));
        pinValue.setMapItem(GraphLoader.GML_NODE_ATTR_PARENT, GmlBuilder.newNumberValue( pin.getCard().getCardId() ));
        pinValue.setMapItem(GraphLoader.GML_NODE_ATTR_TYPE, GmlBuilder.newStringValue( GraphLoader.GML_NODE_TYPE_PIN ));
        pinValue.setMapItem(GraphLoader.GML_NODE_ATTR_POSITION, GmlBuilder.newStringValue( pin.getLocation().name() ));

        return pinValue;
    }

}
