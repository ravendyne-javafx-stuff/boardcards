package com.ravendyne.simplecards.files;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.constellation.graph.generic.gml.GmlBuilder;
import com.ravendyne.constellation.graph.generic.gml.GmlValue;
import com.ravendyne.constellation.graph.generic.gml.GmlValueBuilder;
import com.ravendyne.simplecards.SimpleCardsApp;
import com.ravendyne.simplecards.files.converters.CardConverter;
import com.ravendyne.simplecards.files.converters.ConnectorConverter;
import com.ravendyne.simplecards.files.converters.DrawingBoardConverter;

public class DrawingPersistenceManager {
    public static final String indent = "  ";

	private static Map<IDrawingBoard, DrawingPersistenceManager> instances = new HashMap<>();
	
    private Path currentFilePath;
    IDrawingBoard drawing;
	
	private DrawingPersistenceManager(IDrawingBoard drawing) {
		currentFilePath = null;
		this.drawing = drawing;
	}
	
	public static DrawingPersistenceManager getInstance(IDrawingBoard drawing) {
		DrawingPersistenceManager dpm = instances.get(drawing);
		
		if(dpm == null) {
			dpm = new DrawingPersistenceManager(drawing);
			instances.put(drawing, dpm);
		}
		
		return dpm;
	}

    public void loadFrom(Path filePath) throws IOException {
        load(filePath);

        currentFilePath = filePath;
        AppPreferences.INSTANCE.setLastOpenFile(currentFilePath.toString());
        AppPreferences.INSTANCE.save();

        updateAppTitle();
    }

    private void updateAppTitle() {
        SimpleCardsApp.setTitle(currentFilePath.getFileName().toString());
    }

    public void saveTo(Path filePath) throws IOException {
        save(filePath);

        currentFilePath = filePath;
        AppPreferences.INSTANCE.setLastOpenFile(currentFilePath.toString());
        AppPreferences.INSTANCE.save();

        updateAppTitle();
    }

    private void load(Path filePath) throws IOException {
        List<String> gmlSource = Files.readAllLines(filePath);
        drawing.clean();

        GraphLoader loader = new GraphLoader(drawing);
        loader.load(gmlSource);

        Map<String, GmlValue> attrs = loader.getGraphAttributes();
        DrawingBoardConverter.updateFromGml( drawing, attrs );
    }

    private void save(Path filePath) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos, true);

        GmlValue graphMap = convertToGml();
        GmlBuilder.save(graphMap, out);

        Files.write(filePath, baos.toByteArray(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
    }
    
    public Path getCurrentFilePath() {
    	return currentFilePath;
    }

    public void resetCurrentFilePath() {
        currentFilePath = Paths.get("new.gml").toAbsolutePath().normalize();

        updateAppTitle();
    }
    
    public static String indent(int level) {
        return String.join("", Collections.nCopies(level, indent));
    }

    private GmlValue convertToGml() {
        GmlValueBuilder drawingMap = DrawingBoardConverter.toGml( drawing );

        GmlValueBuilder cardsList = GmlBuilder.newListValue();
        Iterable<ICard> cards = drawing.getCards();
        for(ICard card : cards) {
            GmlValue nodes = CardConverter.toGml((ITitledCard)card);
            cardsList.addAllListItems(nodes);
        }
        drawingMap.setMapItem("node", cardsList);
        
        GmlValueBuilder edgesList = GmlBuilder.newListValue();
        Iterable<IConnector> connectors = drawing.getConnectors();
        for(IConnector connector : connectors) {
            GmlValue edge = ConnectorConverter.toGml(connector);
            edgesList.addListItem(edge);
        }
        drawingMap.setMapItem("edge", edgesList);

        return drawingMap;
    }

}
