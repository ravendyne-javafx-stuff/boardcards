package com.ravendyne.simplecards.files;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.constellation.graph.generic.gml.GmlUtil;
import com.ravendyne.constellation.graph.generic.gml.GmlValue;
import com.ravendyne.constellation.graph.generic.gml.IGraphEdgeConsumer;
import com.ravendyne.constellation.graph.generic.gml.IGraphNodeConsumer;
import com.ravendyne.simplecards.files.converters.CardConverter;
import com.ravendyne.simplecards.files.converters.ConnectorConverter;
import com.ravendyne.simplecards.files.converters.PinConverter;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class GraphLoader {
    public static final String GML_GRAPH_ATTR_WIDTH = "width";
    public static final String GML_GRAPH_ATTR_HEIGHT = "height";

    // NODE
    public static final String GML_NODE_ATTR_TYPE = "type";
    public static final String GML_NODE_ATTR_CARD_TYPE = "cardType";
    public static final String GML_NODE_TYPE_CARD = "card";
    public static final String GML_NODE_TYPE_PIN = "pin";
    // CARD
    public static final String GML_NODE_ATTR_TEXT = "text";
    public static final String GML_NODE_ATTR_X = "x";
    public static final String GML_NODE_ATTR_Y = "y";
    // PIN
    public static final String GML_NODE_ATTR_POSITION = "position";
    public static final String GML_NODE_ATTR_PARENT = "parent";
    public static final String GML_NODE_ATTR_COLOR = "color";
    // CONNECTOR
    public static final String GML_EDGE_ATTR_TYPE = "type";
//    public static final String GML_EDGE_TYPE_LINE = "line";
//    public static final String GML_EDGE_TYPE_CUBIC = "cubic";

    private IDrawingBoard drawingPane;
    private Set<ICard> allCards;
    private Set<IConnector> allConnectors;
    
    private Map<String, GmlValue> graphAttributes;

    public GraphLoader(IDrawingBoard drawingPane) {
        this.drawingPane = drawingPane;
        allCards = new HashSet<>();
        allConnectors = new HashSet<>();
        
        graphAttributes = new HashMap<>();
    }

    public Map<String, GmlValue> getGraphAttributes() {
        return graphAttributes;
    }

    public Iterable< ICard > getCards() {
        return allCards;
    }

    public Iterable< IConnector > getConnectors() {
        return allConnectors;
    }

    public void load(List<String> gmlSource) {
        Map<String, GmlValue> graphAsGmlValues = GmlUtil.loadGml(gmlSource);
        load(graphAsGmlValues);
    }

    public void load(Map<String, GmlValue> graph) {
        loadGraph(graph);
    }
    
    static class EdgeAttrs {
        String source;
        String target;
        Map<String, GmlValue> attrs;
    }
    
    private void loadGraph(Map<String, GmlValue> graph) {
        Map<String, Map<String, GmlValue>> gmlPins = new HashMap<>();
        Set<EdgeAttrs> gmlEdges = new HashSet<>();

        Map<String, ICard> cardsMap = new HashMap<>();
        Map<String, IPin> pinsMap = new HashMap<>();
        Set<IConnector> connectorSet = new HashSet<>();
        
        Consumer<Map<String, GmlValue>> graphAttrsConsumer = (attrs) -> {
            graphAttributes.putAll(attrs);
        };

        // create boxes
        IGraphNodeConsumer nodeConsumer = (id, attrs) -> {
            GmlValue typeValue = attrs.get(GML_NODE_ATTR_TYPE);
            String type = GML_NODE_TYPE_CARD;
            if(typeValue != null) {
                type = typeValue.getString();
            }

            if( type.equals(GML_NODE_TYPE_CARD) ) {
                // create cards
                ICard card = CardConverter.fromGml(drawingPane, attrs);
                cardsMap.put(id, card);
                allCards.add(card);
            }

            if( type.equals(GML_NODE_TYPE_PIN) ) {
                // collect pins
                String parent = String.valueOf(attrs.get(GML_NODE_ATTR_PARENT));
                String position = attrs.get(GraphLoader.GML_NODE_ATTR_POSITION).getString();
                
                BooleanProperty exists = new SimpleBooleanProperty(false);
                gmlPins.values().forEach((val) -> {
                    String _parent = String.valueOf(val.get(GML_NODE_ATTR_PARENT));
                    String _position = val.get(GraphLoader.GML_NODE_ATTR_POSITION).getString();
                    if(parent.equals(_parent) && position.equals(_position)) {
                        exists.set(true);
                    }
                });

                System.out.println("considering " + parent + ":" + position);
                if(!exists.get()) {
                    gmlPins.put(id, attrs);
                    System.out.println("accepted");
                } else {
                    System.out.println("rejected");
                }
            }

        };
        
        IGraphNodeConsumer isolatedNodeConsumer = (x, attrs) -> {
            // ignore isolated nodes
            System.out.println("isolated node ignored: " + attrs);
        };

        // collect edges
        IGraphEdgeConsumer edgeConsumer = (source, target, attrs) -> {
            EdgeAttrs edge = new EdgeAttrs();
            edge.source = source;
            edge.target = target;
            edge.attrs = attrs;
            gmlEdges.add(edge);
        };

        GmlUtil.parseGraph(graph, graphAttrsConsumer, nodeConsumer, edgeConsumer, isolatedNodeConsumer);
        
        // create pins and add them to cards
        gmlPins.keySet().forEach((id) -> {
            Map<String, GmlValue> attrs = gmlPins.get(id);

            final GmlValue parentValue = attrs.get(GML_NODE_ATTR_PARENT);
            String parent = String.valueOf(parentValue);
            ICard parentCard = cardsMap.get(parent);
            
            IPin pin = PinConverter.fromGml(parentCard, attrs);
            if(pin == null) {
                System.out.println("null pin for " + attrs);
            }

            pinsMap.put(id, pin);
        });
        
        // create connectors
        gmlEdges.forEach((edge) -> {
            IPin sourcePin = pinsMap.get(edge.source);
            IPin targetPin = pinsMap.get(edge.target);

            IConnector connector = ConnectorConverter.fromGml(drawingPane, sourcePin, targetPin, edge.attrs);

            allConnectors.add(connector);
            connectorSet.add(connector);
        });
    }
}
