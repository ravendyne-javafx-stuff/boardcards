package com.ravendyne.simplecards.deck;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.simplecards.deck.card.SCCardBuilder;
import com.ravendyne.simplecards.deck.card.SCCardBuilder.DocsCardType;
import com.ravendyne.simplecards.deck.connector.SCConnectorBuilder;
import com.ravendyne.simplecards.deck.connector.SCConnectorBuilder.ConnectorType;
import com.ravendyne.simplecards.deck.pin.SCPinBuilder;

public class SimpleCardsCreator implements IDefaultsBuilder {
    private static SimpleCardsCreator instance;
    
    private static ConnectorType defaultConnectorType = ConnectorType.LineArrow;
    
    private static DocsCardType defaultCardType = DocsCardType.Rectangle;

    private SimpleCardsCreator() {}
    
    public static SimpleCardsCreator getInstance() {
        if(instance == null) {
            instance = new SimpleCardsCreator();
        }
        
        return instance;
    }

    public SCPinBuilder pin() {
        return new SCPinBuilder();
    }

    public SCCardBuilder card() {
        return new SCCardBuilder();
    }

    public SCConnectorBuilder connector() {
        return new SCConnectorBuilder();
    }

    public void setDefaultConnectorType(ConnectorType type) {
        defaultConnectorType = type;
    }

    public void setDefaultCardType(DocsCardType type) {
        defaultCardType = type;
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        ITitledCard card = card()
                .setDrawing(drawing)
                .setTitle("")
                .setType(defaultCardType)
                .build();
        card.setTitle("#" + card.getCardId());

        switch(defaultCardType) {
        case Circle:
            pin().setCard(card).setLocation(Location.Center).build();
            break;
        case Rectangle:
        case Text:
            pin().setCard(card).setLocation(Location.Top).build();
            pin().setCard(card).setLocation(Location.Bottom).build();
            pin().setCard(card).setLocation(Location.Right).build();
            pin().setCard(card).setLocation(Location.Left).build();
            break;
        }
        
        return card;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        IConnector connector = connector()
                .setDrawingBoard(drawing)
                .setSource(source)
                .setTarget(target)
                .setType(defaultConnectorType)
                .build();
        return connector;
    }

}
