package com.ravendyne.simplecards.deck;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.samples.SampleDrawingPane;

public class SimpleCardsDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return SimpleCardsCreator.getInstance();
    }
}
