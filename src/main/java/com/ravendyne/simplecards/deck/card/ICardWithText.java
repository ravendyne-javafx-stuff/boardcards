package com.ravendyne.simplecards.deck.card;


public interface ICardWithText {

    String getText();

    void setText(String text);

}
