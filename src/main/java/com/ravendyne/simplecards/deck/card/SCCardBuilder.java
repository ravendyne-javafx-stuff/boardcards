package com.ravendyne.simplecards.deck.card;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;

public class SCCardBuilder {
    private static final boolean bDebug = false;
    
    public static enum DocsCardType {
        Circle,
        Rectangle,
        Text
    }
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    protected String pText;
    
    DocsCardType pCardType;

    public SCCardBuilder() {
        pCardType = DocsCardType.Rectangle;
    }

    public SCCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public SCCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

    public SCCardBuilder setText(String text) {
        pText = text;
        return this;
    }

    public SCCardBuilder setType(DocsCardType type) {
        Objects.requireNonNull(type);
        pCardType = type;
        return this;
    }

    public ITitledCard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ITitledCard card = null;
        switch(pCardType) {
        case Circle:
            card = new SCCircleCard( pDrawing, pTitle );
            break;
        case Rectangle:
            card = new SCRectangleCard( pDrawing, pTitle );
            break;
        case Text:
            card = new SCRectangleCardWithText(pDrawing, pTitle, pText);
            break;
        }
        card.buildUI();
        pDrawing.addCard(card);
        if(bDebug) System.out.println("added card #" + card.getCardId());

        return card;
    }
    
    public DocsCardType getCardType(ICard card) {
        DocsCardType cardType = DocsCardType.Rectangle;

        if(card instanceof SCCircleCard) {
            cardType = DocsCardType.Circle;
        } else if(card instanceof SCRectangleCard) {
            cardType = DocsCardType.Rectangle;
        } else if(card instanceof SCRectangleCardWithText) {
            cardType = DocsCardType.Text;
        }

        return cardType;
    }

}
