package com.ravendyne.simplecards.deck.card;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.drawing.AbstractCard;
import com.ravendyne.cardsboard.stackofcards.circles.deck.pin.CirclesTransparentPin;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;

public class SCCircleCard extends AbstractCard implements ITitledCard {
    
    Label titleLabel;
    
    IPin topPin;
    IPin bottomPin;
    IPin leftPin;
    IPin rightPin;

    SCCircleCard(IDrawingBoard drawing, String title) {
        super(drawing);
        
        titleLabel = new Label( title );
    }

    @Override
    protected Pane createRootUINode() {
        StackPane cardPane = new StackPane();
        
        titleLabel.setTranslateY(-20);

        cardPane.getChildren().add(getContentNode());
        cardPane.getChildren().add(titleLabel);
        
        cardPane.getStyleClass().add("circle");

        return cardPane;
    }

    @Override
    protected Node createContentUINode() {
        Circle uiCircle = new Circle();

        uiCircle.setRadius(CirclesTransparentPin.connectorSize + 2);

        return uiCircle;
    }

    @Override
    public String getTitle() {
        return titleLabel.getText();
    }

    @Override
    public void setTitle(String text) {
        titleLabel.setText(text);
    }

    @Override
    public void discard() {
        super.discard();

        // since we added references to these two to our root UI node
        // it helps with memory leak debugging to nullify it here
        titleLabel = null;
    }

    @Override
    protected void removePinFromUI( IPin pin ) {
        getRootNode().getChildren().remove( pin.getRootNode() );
    }

    @Override
    protected void addPinToUI(IPin pin) {
        IPin oldPin = null;

        switch(pin.getLocation()) {
        case Top:
        case Bottom:
        case Left:
        case Right:
            throw new UnsupportedOperationException("Can only add pin to card center.");
        case Center:
            oldPin = bottomPin;
            bottomPin = pin;
            break;
        }
        
        if(oldPin != null) {
            throw new UnsupportedOperationException("Can't add pin to already populated location");
        }

        setPinLocation(pin);

        getRootNode().getChildren().add( pin.getRootNode() );
    }

    private boolean setPinLocation(IPin pin) {
        switch (pin.getLocation()) {
        case Top:
        case Right:
        case Bottom:
        case Left:
            throw new UnsupportedOperationException("Can only set pin location to card center.");
        case Center:
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER);
            break;
        }
        
        return true;
    }

}
