package com.ravendyne.simplecards.deck.connector;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.drawing.AbstractConnector;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.shape.Line;

public class LineConnector extends AbstractConnector {
    
    private Line meLine;
    
    LineConnector(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        super(drawing, sourcePin, targetPin);
    }
    
    @Override
    protected Node createRootUINode() {
        return meLine;
    }

    @Override
    protected Node createConnectorShapeUINode() {
        meLine = new Line();

        return meLine;
    }

    @Override
    public void updatePosition() {
        Point2D sourceConnectionPointOnDrawing = source.getAttachmentPosition();
        Point2D targetConnectionPointOnDrawing = target.getAttachmentPosition();

        final double startX = sourceConnectionPointOnDrawing.getX();
        final double startY = sourceConnectionPointOnDrawing.getY();
        final double endX = targetConnectionPointOnDrawing.getX();
        final double endY = targetConnectionPointOnDrawing.getY();
        
        meLine.setStartX(startX);
        meLine.setStartY(startY);
        meLine.setEndX(endX);
        meLine.setEndY(endY);
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[source= " + source.getPinId() + ", target=" + target.getPinId() + "]";
    }
}
