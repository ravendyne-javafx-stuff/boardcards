package com.ravendyne.simplecards.deck.connector;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.drawing.AbstractConnector;

import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

public class BezierConnectorWithArrow extends AbstractConnector {

    CubicCurve connectorCurve;
    Polygon arrowHead;
    
    BezierConnectorWithArrow(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        super(drawing, sourcePin, targetPin);
        
    }

    @Override
    protected Node createRootUINode() {
        Group connectorGroup = new Group();

        arrowHead = new Polygon();
        arrowHead.getPoints().addAll(new Double[] {0.0, 0.0, 10.0, 0.0, 5.0, 20.0});
        arrowHead.getStyleClass().add(Styles.CONNECTOR_ARROW_HEAD_STYLE);
        arrowHead.setStrokeType(StrokeType.INSIDE);
        // this is not needed when using stroke type INSIDE, but we leave it here for illustration
        arrowHead.setStrokeLineJoin(StrokeLineJoin.ROUND);
        
        connectorGroup.getChildren().add(connectorCurve);
        connectorGroup.getChildren().add(arrowHead);
        
        precalc();
        
        return connectorGroup;
    }

    @Override
    protected Node createConnectorShapeUINode() {
        connectorCurve = new CubicCurve();

        connectorCurve.setFill(null);

        return connectorCurve;
    }

    double deltaControlX1;
    double deltaControlY1;
    double deltaControlX2;
    double deltaControlY2;
    double deltaEndX;
    double deltaEndY;

    double arrowDeltaX;
    double arrowDeltaY;
    
    // we assume pin position stays the same
    private void precalc() {
        
        Location sourceLocation = source.getLocation();
        Location targetLocation = target.getLocation();

        final int delta = 60;

        switch(sourceLocation) {
            case Top:
                deltaControlX1 = 0;
                deltaControlY1 = - delta;
                break;
            case Right:
                deltaControlX1 = + delta;
                deltaControlY1 = 0;
                break;
            case Bottom:
                deltaControlX1 = 0;
                deltaControlY1 = + delta;
                break;
            case Left:
                deltaControlX1 = - delta;
                deltaControlY1 = 0;
                break;
            case Center:
                throw new UnsupportedOperationException("Bezier connector can't connect to centered pin, only to edge pins.");
        }

        switch(targetLocation) {
            case Top:
                deltaControlX2 = 0;
                deltaControlY2 = - delta;
                arrowHead.setRotate(0.0);
                break;
            case Right:
                deltaControlX2 = + delta;
                deltaControlY2 = 0;
                arrowHead.setRotate(90.0);
                break;
            case Bottom:
                deltaControlX2 = 0;
                deltaControlY2 = + delta;
                arrowHead.setRotate(180.0);
                break;
            case Left:
                deltaControlX2 = - delta;
                deltaControlY2 = 0;
                arrowHead.setRotate(-90.0);
                break;
            case Center:
                throw new UnsupportedOperationException("Bezier connector can't connect to centered pin, only to edge pins.");
        }
        
        // we use boundInParent since we need to know width/height after arrow head has been rotated
        double arrowWidth = arrowHead.getBoundsInParent().getWidth();
        double arrowHeight = arrowHead.getBoundsInParent().getHeight();

        switch(targetLocation) {
            case Top:
                arrowDeltaX = - arrowWidth / 2.0;
                arrowDeltaY = - arrowHeight;
                deltaEndX = 0;
                deltaEndY = - arrowHeight;
                break;
            case Right:
                arrowDeltaX = 0;
                arrowDeltaY = - arrowHeight / 2.0;
                deltaEndX = arrowWidth;
                deltaEndY = 0;
                break;
            case Bottom:
                arrowDeltaX = - arrowWidth / 2.0;
                arrowDeltaY = 0;
                deltaEndX = 0;
                deltaEndY = arrowHeight;
                break;
            case Left:
                arrowDeltaX = - arrowWidth;
                arrowDeltaY = - arrowHeight / 2.0;
                deltaEndX = - arrowWidth;
                deltaEndY = 0;
                break;
            case Center:
                throw new UnsupportedOperationException("Bezier connector can't connect to centered pin, only to edge pins.");
        }

        // adjust for whatever translation was added after arrow has been
        // rotated around its geometric center (default pivot point for rotate() method)
        arrowDeltaX -= arrowHead.getBoundsInParent().getMinX();
        arrowDeltaY -= arrowHead.getBoundsInParent().getMinY();
    }

    @Override
    public void updatePosition() {
        Point2D sourceConnectionPointOnDrawing = source.getAttachmentPosition();
        Point2D targetConnectionPointOnDrawing = target.getAttachmentPosition();

        final double startX = sourceConnectionPointOnDrawing.getX();
        final double startY = sourceConnectionPointOnDrawing.getY();
        final double endX = targetConnectionPointOnDrawing.getX();
        final double endY = targetConnectionPointOnDrawing.getY();

        connectorCurve.setStartX(startX);
        connectorCurve.setStartY(startY);
        connectorCurve.setControlX1(startX + deltaControlX1);
        connectorCurve.setControlY1(startY + deltaControlY1);
        connectorCurve.setControlX2(endX + deltaControlX2);
        connectorCurve.setControlY2(endY + deltaControlY2);
        connectorCurve.setEndX(endX + deltaEndX);
        connectorCurve.setEndY(endY + deltaEndY);

        arrowHead.relocate(endX + arrowDeltaX, endY + arrowDeltaY);
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[source= " + source.getPinId() + ", target=" + target.getPinId() + "]";
    }
}
