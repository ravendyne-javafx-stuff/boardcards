package com.ravendyne.simplecards.deck.connector;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.drawing.AbstractConnector;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.shape.CubicCurve;

public class BezierConnector extends AbstractConnector {
    
    private CubicCurve meShape;

    BezierConnector(IDrawingBoard drawing, IPin sourcePin, IPin targetPin) {
        super(drawing, sourcePin, targetPin);
    }
    
    @Override
    protected Node createRootUINode() {
        
        precalc();
        
        return meShape;
    }

    @Override
    protected Node createConnectorShapeUINode() {
        meShape = new CubicCurve();

        meShape.setFill(null);

        return meShape;
    }

    double deltaX1;
    double deltaY1;
    double deltaX2;
    double deltaY2;
    
    // we assume pin position stays the same
    private void precalc() {
        
        Location sourceLocation = source.getLocation();
        Location targetLocation = target.getLocation();

        final int delta = 40;

        switch(sourceLocation) {
            case Top:
                deltaX1 = 0;
                deltaY1 = - delta;
                break;
            case Right:
                deltaX1 = + delta;
                deltaY1 = 0;
                break;
            case Bottom:
                deltaX1 = 0;
                deltaY1 = + delta;
                break;
            case Left:
                deltaX1 = - delta;
                deltaY1 = 0;
                break;
            case Center:
                throw new UnsupportedOperationException("Bezier connector can't connect to centered pin, only to edge pins.");
        }

        switch(targetLocation) {
            case Top:
                deltaX2 = 0;
                deltaY2 = - delta;
                break;
            case Right:
                deltaX2 = + delta;
                deltaY2 = 0;
                break;
            case Bottom:
                deltaX2 = 0;
                deltaY2 = + delta;
                break;
            case Left:
                deltaX2 = - delta;
                deltaY2 = 0;
                break;
            case Center:
                throw new UnsupportedOperationException("Bezier connector can't connect to centered pin, only to edge pins.");
        }
    }

    @Override
    public void updatePosition() {
        Point2D sourceConnectionPointOnDrawing = source.getAttachmentPosition();
        Point2D targetConnectionPointOnDrawing = target.getAttachmentPosition();
        
        CubicCurve curve = (CubicCurve) getRootNode();

        final double startX = sourceConnectionPointOnDrawing.getX();
        final double startY = sourceConnectionPointOnDrawing.getY();
        final double endX = targetConnectionPointOnDrawing.getX();
        final double endY = targetConnectionPointOnDrawing.getY();

        curve.setStartX(startX);
        curve.setStartY(startY);
        curve.setControlX1(startX + deltaX1);
        curve.setControlY1(startY + deltaY1);
        curve.setControlX2(endX + deltaX2);
        curve.setControlY2(endY + deltaY2);
        curve.setEndX(endX);
        curve.setEndY(endY);
    }

    @Override
    public String toString() {
        String klassName = getClass().getName();
        String simpleName = klassName.substring(klassName.lastIndexOf('.')+1);

        return simpleName + "[source= " + source.getPinId() + ", target=" + target.getPinId() + "]";
    }
}
