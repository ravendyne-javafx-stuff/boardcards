package com.ravendyne.simplecards.deck.pin;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

import javafx.scene.paint.Color;

public class SCPinBuilder {

    ICard pCard;
    Color pColor;
    Location pLocation;
    
    public SCPinBuilder() {
        pColor = Color.web("#00a8f4");
    }
    
    public SCPinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public SCPinBuilder setColor(Color color) {
        Objects.requireNonNull(color);
        pColor = color;
        return this;
    }

    public SCPinBuilder setColor(String colorString) {
        pColor = Color.web(colorString);
        return this;
    }

    public SCPinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(pCard);
        Objects.requireNonNull(pColor);
        Objects.requireNonNull(pLocation);

        IPin pin = new SCTransparentPin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }


}
