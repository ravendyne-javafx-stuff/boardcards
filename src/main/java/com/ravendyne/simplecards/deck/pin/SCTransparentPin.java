package com.ravendyne.simplecards.deck.pin;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.drawing.AbstractPin;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

public class SCTransparentPin extends AbstractPin {
    public static final double connectorSize = 8;

    Pane pinUI;
    private double hotspotXParam;
    private double hotspotYParam;

    SCTransparentPin(ICard card) {
        super(card);
    }
    
    private Pane getPinUI() {
        if(pinUI == null) {
            pinUI = new Pane();
            pinUI.setPrefWidth(connectorSize);
            pinUI.setPrefHeight(connectorSize);
//            pinUI.getStyleClass().add("debug-region");
        }
        
        return pinUI;
    }

    @Override
    protected Region createRootUINode() {
        return getPinUI();
    }

    @Override
    protected Node createConnectionPoinUINode() {
        return getPinUI();
    }

    @Override
    public void setLocation(Location location) {
        super.setLocation(location);
        setDimensions();
        setHotspotData();
    }

    private void setDimensions() {
        switch(getLocation()) {
        case Top:
        case Bottom:
            pinUI.setMaxHeight(connectorSize);
            pinUI.setMaxWidth(-1);
            break;
        case Left:
        case Right:
            pinUI.setMaxHeight(-1);
            pinUI.setMaxWidth(connectorSize);
            break;
        case Center:
            pinUI.setMaxHeight(connectorSize);
            pinUI.setMaxWidth(connectorSize);
            break;
        }
    }
    
    private void setHotspotData() {
        switch(getLocation()) {
        case Top:
            hotspotXParam = 0.5;
            hotspotYParam = 0.0;
            break;
        case Bottom:
            hotspotXParam = 0.5;
            hotspotYParam = 1.0;
            break;
        case Left:
            hotspotXParam = 0.0;
            hotspotYParam = 0.5;
            break;
        case Right:
            hotspotXParam = 1.0;
            hotspotYParam = 0.5;
            break;
        case Center:
            hotspotXParam = 0.5;
            hotspotYParam = 0.5;
            break;
        }
    }

    @Override
    public Point2D getAttachmentPositionLocal() {
        return new Point2D(hotspotXParam * getConnectorAttachmentNode().getLayoutBounds().getWidth(), hotspotYParam * getConnectorAttachmentNode().getLayoutBounds().getHeight());
    }

}
