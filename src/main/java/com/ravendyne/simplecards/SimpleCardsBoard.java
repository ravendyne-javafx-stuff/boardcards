package com.ravendyne.simplecards;

import java.io.IOException;
import java.nio.file.Paths;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.simplecards.deck.SimpleCardsDrawingPane;
import com.ravendyne.simplecards.files.AppPreferences;
import com.ravendyne.simplecards.files.DrawingPersistenceManager;
import com.ravendyne.simplecards.ui.CardEditForm;
import com.ravendyne.simplecards.ui.CardToolbox;
import com.ravendyne.simplecards.ui.DrawingUtil;
import com.ravendyne.simplecards.ui.TopToolbox;

import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class SimpleCardsBoard {
    
    // TOP
    @Inject HBox topBox;

    // LEFT
    @Inject SplitPane boxLeft;

    // CENTER
    @Inject ScrollPane drawingScrollPane;
    
    IDrawingBoard drawing;
    DrawingPersistenceManager persistenceManager;

    public void run( Stage primaryStage ) {
        drawing = new SimpleCardsDrawingPane();
        persistenceManager = DrawingPersistenceManager.getInstance(drawing);

        drawingScrollPane.setContent(drawing.getNode());

        CardEditForm editForm = new CardEditForm();
        CardToolbox cardToolbox = new CardToolbox();
        TopToolbox topToolbox = new TopToolbox();

        editForm.setActiveDrawing(drawing);
        cardToolbox.setActiveDrawing(drawing);
        topToolbox.setActiveDrawing(drawing);

        // makes the toolbox container fill all the space horizontally
        HBox.setHgrow( topToolbox.getNode(), Priority.ALWAYS );
        topBox.getChildren().add(topToolbox.getNode());
        
        boxLeft.getItems().add(cardToolbox.getNode());
        boxLeft.getItems().add(editForm.getNode());

        String lastOpenFile = AppPreferences.INSTANCE.getLastOpenFile();
        if(lastOpenFile != null) {
            try {
            	persistenceManager.loadFrom(Paths.get(lastOpenFile));
            } catch (IOException e) {
                DrawingUtil.clearDrawing( drawing );
            }
        }
    }
}
