graph [
  node [
    x 14
    y 19
    id 45
    label "FBP"
    text "Components"
    type "card"
  ]
  node [
    parent 45
    id 156
    position "left"
    type "pin"
  ]
  node [
    parent 45
    id 160
    position "top"
    type "pin"
  ]
  node [
    parent 45
    id 145
    position "right"
    type "pin"
  ]
  node [
    parent 45
    id 146
    position "bottom"
    type "pin"
  ]
  node [
    x 853
    y 639
    id 48
    label "file parser"
    text ""
    type "card"
  ]
  node [
    parent 48
    id 77
    position "left"
    type "pin"
  ]
  node [
    parent 48
    id 78
    position "right"
    type "pin"
  ]
  node [
    parent 48
    id 79
    position "top"
    type "pin"
  ]
  node [
    parent 48
    id 80
    position "bottom"
    type "pin"
  ]
  node [
    x 954
    y 159
    id 38
    label "in C"
    text "- coop engine -> uC
- transpiler c -> Java + sim + dbg
- editor with components & code gen"
    type "card"
  ]
  node [
    parent 38
    id 69
    position "top"
    type "pin"
  ]
  node [
    parent 38
    id 70
    position "left"
    type "pin"
  ]
  node [
    parent 38
    id 68
    position "bottom"
    type "pin"
  ]
  node [
    parent 38
    id 155
    position "right"
    type "pin"
  ]
  node [
    x 571
    y 596
    id 42
    label "java three"
    text "-"
    type "card"
  ]
  node [
    parent 42
    id 54
    position "bottom"
    type "pin"
  ]
  node [
    parent 42
    id 72
    position "top"
    type "pin"
  ]
  node [
    parent 42
    id 55
    position "right"
    type "pin"
  ]
  node [
    parent 42
    id 56
    position "left"
    type "pin"
  ]
  node [
    x 192
    y 147
    id 46
    label "File writer"
    text ""
    type "card"
  ]
  node [
    parent 46
    id 101
    position "right"
    type "pin"
  ]
  node [
    parent 46
    id 102
    position "bottom"
    type "pin"
  ]
  node [
    parent 46
    id 109
    position "left"
    type "pin"
  ]
  node [
    parent 46
    id 103
    position "top"
    type "pin"
  ]
  node [
    x 720
    y 425
    id 26
    label "with gpu"
    text ""
    type "card"
  ]
  node [
    parent 26
    id 134
    position "right"
    type "pin"
  ]
  node [
    parent 26
    id 51
    position "top"
    type "pin"
  ]
  node [
    parent 26
    id 59
    position "bottom"
    type "pin"
  ]
  node [
    parent 26
    id 165
    position "left"
    type "pin"
  ]
  node [
    x 220
    y 576
    id 43
    label "new card"
    text "- create second order (velocity driven)
"
    type "card"
  ]
  node [
    parent 43
    id 60
    position "left"
    type "pin"
  ]
  node [
    parent 43
    id 62
    position "bottom"
    type "pin"
  ]
  node [
    parent 43
    id 63
    position "right"
    type "pin"
  ]
  node [
    parent 43
    id 58
    position "top"
    type "pin"
  ]
  node [
    x 571
    y 234
    id 21
    label "particle simulation"
    text ""
    type "card"
  ]
  node [
    parent 21
    id 71
    position "top"
    type "pin"
  ]
  node [
    parent 21
    id 50
    position "bottom"
    type "pin"
  ]
  node [
    parent 21
    id 164
    position "left"
    type "pin"
  ]
  node [
    parent 21
    id 163
    position "right"
    type "pin"
  ]
  node [
    x 241
    y 506
    id 22
    label "new card"
    text "- test first order (position driven)"
    type "card"
  ]
  node [
    parent 22
    id 94
    position "bottom"
    type "pin"
  ]
  node [
    parent 22
    id 95
    position "left"
    type "pin"
  ]
  node [
    parent 22
    id 93
    position "right"
    type "pin"
  ]
  node [
    parent 22
    id 96
    position "top"
    type "pin"
  ]
  node [
    x 573
    y 28
    id 32
    label "FBP"
    text "-"
    type "card"
  ]
  node [
    parent 32
    id 83
    position "left"
    type "pin"
  ]
  node [
    parent 32
    id 84
    position "top"
    type "pin"
  ]
  node [
    parent 32
    id 81
    position "bottom"
    type "pin"
  ]
  node [
    parent 32
    id 82
    position "right"
    type "pin"
  ]
  node [
    x 951
    y 41
    id 25
    label "in Java"
    text "- dbg view
- POC
- more components
- code generator"
    type "card"
  ]
  node [
    parent 25
    id 108
    position "bottom"
    type "pin"
  ]
  node [
    parent 25
    id 105
    position "top"
    type "pin"
  ]
  node [
    parent 25
    id 107
    position "right"
    type "pin"
  ]
  node [
    parent 25
    id 106
    position "left"
    type "pin"
  ]
  node [
    x 1073
    y 534
    id 30
    label "air plane + launcher"
    text ""
    type "card"
  ]
  node [
    parent 30
    id 89
    position "top"
    type "pin"
  ]
  node [
    parent 30
    id 91
    position "bottom"
    type "pin"
  ]
  node [
    parent 30
    id 92
    position "right"
    type "pin"
  ]
  node [
    parent 30
    id 90
    position "left"
    type "pin"
  ]
  node [
    x 570
    y 747
    id 36
    label "dynamo fx"
    text "-"
    type "card"
  ]
  node [
    parent 36
    id 123
    position "left"
    type "pin"
  ]
  node [
    parent 36
    id 111
    position "bottom"
    type "pin"
  ]
  node [
    parent 36
    id 110
    position "right"
    type "pin"
  ]
  node [
    parent 36
    id 114
    position "top"
    type "pin"
  ]
  node [
    x 178
    y 217
    id 29
    label "IP shredder"
    text ""
    type "card"
  ]
  node [
    parent 29
    id 117
    position "bottom"
    type "pin"
  ]
  node [
    parent 29
    id 118
    position "left"
    type "pin"
  ]
  node [
    parent 29
    id 115
    position "right"
    type "pin"
  ]
  node [
    parent 29
    id 116
    position "top"
    type "pin"
  ]
  node [
    x 1072
    y 465
    id 37
    label "sw for above to program them"
    text ""
    type "card"
  ]
  node [
    parent 37
    id 158
    position "top"
    type "pin"
  ]
  node [
    parent 37
    id 162
    position "right"
    type "pin"
  ]
  node [
    parent 37
    id 147
    position "bottom"
    type "pin"
  ]
  node [
    parent 37
    id 161
    position "left"
    type "pin"
  ]
  node [
    x 16
    y 739
    id 33
    label "new card"
    text "- write uC code & test with the rig"
    type "card"
  ]
  node [
    parent 33
    id 119
    position "top"
    type "pin"
  ]
  node [
    parent 33
    id 121
    position "right"
    type "pin"
  ]
  node [
    parent 33
    id 144
    position "bottom"
    type "pin"
  ]
  node [
    parent 33
    id 128
    position "left"
    type "pin"
  ]
  node [
    x 572
    y 164
    id 34
    label "graph"
    text "-"
    type "card"
  ]
  node [
    parent 34
    id 137
    position "left"
    type "pin"
  ]
  node [
    parent 34
    id 139
    position "right"
    type "pin"
  ]
  node [
    parent 34
    id 140
    position "top"
    type "pin"
  ]
  node [
    parent 34
    id 138
    position "bottom"
    type "pin"
  ]
  node [
    x 853
    y 705
    id 23
    label "file viewer"
    text ""
    type "card"
  ]
  node [
    parent 23
    id 127
    position "top"
    type "pin"
  ]
  node [
    parent 23
    id 113
    position "bottom"
    type "pin"
  ]
  node [
    parent 23
    id 112
    position "right"
    type "pin"
  ]
  node [
    parent 23
    id 120
    position "left"
    type "pin"
  ]
  node [
    x 87
    y 415
    id 47
    label "bezier stepper driver"
    text "-"
    type "card"
  ]
  node [
    parent 47
    id 124
    position "top"
    type "pin"
  ]
  node [
    parent 47
    id 125
    position "bottom"
    type "pin"
  ]
  node [
    parent 47
    id 126
    position "right"
    type "pin"
  ]
  node [
    parent 47
    id 122
    position "left"
    type "pin"
  ]
  node [
    x 208
    y 82
    id 41
    label "File reader"
    text ""
    type "card"
  ]
  node [
    parent 41
    id 97
    position "top"
    type "pin"
  ]
  node [
    parent 41
    id 99
    position "left"
    type "pin"
  ]
  node [
    parent 41
    id 100
    position "right"
    type "pin"
  ]
  node [
    parent 41
    id 98
    position "bottom"
    type "pin"
  ]
  node [
    x 1044
    y 312
    id 49
    label "goals"
    text "- uC prog env
- stepper + drives
- CNC
  - sorter
  - cutter
  - assembler
  - packer"
    type "card"
  ]
  node [
    parent 49
    id 104
    position "right"
    type "pin"
  ]
  node [
    parent 49
    id 135
    position "top"
    type "pin"
  ]
  node [
    parent 49
    id 133
    position "bottom"
    type "pin"
  ]
  node [
    parent 49
    id 136
    position "left"
    type "pin"
  ]
  node [
    x 145
    y 284
    id 31
    label "App"
    text "HTML praser app as FBP network"
    type "card"
  ]
  node [
    parent 31
    id 131
    position "left"
    type "pin"
  ]
  node [
    parent 31
    id 129
    position "top"
    type "pin"
  ]
  node [
    parent 31
    id 130
    position "right"
    type "pin"
  ]
  node [
    parent 31
    id 132
    position "bottom"
    type "pin"
  ]
  node [
    x 571
    y 670
    id 44
    label "GML"
    text "- file"
    type "card"
  ]
  node [
    parent 44
    id 67
    position "right"
    type "pin"
  ]
  node [
    parent 44
    id 65
    position "left"
    type "pin"
  ]
  node [
    parent 44
    id 64
    position "bottom"
    type "pin"
  ]
  node [
    parent 44
    id 66
    position "top"
    type "pin"
  ]
  node [
    x 760
    y 323
    id 35
    label "byte array impl"
    text ""
    type "card"
  ]
  node [
    parent 35
    id 151
    position "top"
    type "pin"
  ]
  node [
    parent 35
    id 148
    position "left"
    type "pin"
  ]
  node [
    parent 35
    id 149
    position "bottom"
    type "pin"
  ]
  node [
    parent 35
    id 150
    position "right"
    type "pin"
  ]
  node [
    x 517
    y 326
    id 40
    label "gravity sim"
    text ""
    type "card"
  ]
  node [
    parent 40
    id 85
    position "bottom"
    type "pin"
  ]
  node [
    parent 40
    id 86
    position "right"
    type "pin"
  ]
  node [
    parent 40
    id 88
    position "top"
    type "pin"
  ]
  node [
    parent 40
    id 87
    position "left"
    type "pin"
  ]
  node [
    x 198
    y 672
    id 39
    label "new card"
    text "- test 2nd order"
    type "card"
  ]
  node [
    parent 39
    id 142
    position "top"
    type "pin"
  ]
  node [
    parent 39
    id 143
    position "left"
    type "pin"
  ]
  node [
    parent 39
    id 61
    position "bottom"
    type "pin"
  ]
  node [
    parent 39
    id 141
    position "right"
    type "pin"
  ]
  node [
    x 572
    y 96
    id 28
    label "FX blueprints"
    text "-"
    type "card"
  ]
  node [
    parent 28
    id 52
    position "bottom"
    type "pin"
  ]
  node [
    parent 28
    id 53
    position "top"
    type "pin"
  ]
  node [
    parent 28
    id 159
    position "left"
    type "pin"
  ]
  node [
    parent 28
    id 157
    position "right"
    type "pin"
  ]
  node [
    x 486
    y 423
    id 24
    label "with array"
    text ""
    type "card"
  ]
  node [
    parent 24
    id 73
    position "top"
    type "pin"
  ]
  node [
    parent 24
    id 76
    position "right"
    type "pin"
  ]
  node [
    parent 24
    id 74
    position "left"
    type "pin"
  ]
  node [
    parent 24
    id 75
    position "bottom"
    type "pin"
  ]
  edge [
    source 125
    type "cubic"
    target 60
  ]
  edge [
    source 146
    type "cubic"
    target 109
  ]
  edge [
    source 125
    type "cubic"
    target 95
  ]
  edge [
    source 67
    type "cubic"
    target 120
  ]
  edge [
    source 50
    type "cubic"
    target 151
  ]
  edge [
    source 146
    type "cubic"
    target 118
  ]
  edge [
    source 85
    type "cubic"
    target 51
  ]
  edge [
    source 146
    type "cubic"
    target 99
  ]
  edge [
    source 125
    type "cubic"
    target 119
  ]
  edge [
    source 50
    type "cubic"
    target 88
  ]
  edge [
    source 85
    type "cubic"
    target 73
  ]
  edge [
    source 82
    type "cubic"
    target 70
  ]
  edge [
    source 146
    type "cubic"
    target 131
  ]
  edge [
    source 82
    type "cubic"
    target 106
  ]
  edge [
    source 67
    type "cubic"
    target 77
  ]
  edge [
    source 125
    type "cubic"
    target 143
  ]
  width 1306
  height 856
]
