package com.ravendyne.simplecards.ui;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.managers.CardSelectionManager;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class DrawingEditUtil {

    private DrawingEditUtil() {}

    public static void newCard(IDrawingBoard drawing) {

        ICard newCard = drawing.getDefaultsBuilder().newDefaultCard(drawing);
        CardSelectionManager.getInstance(drawing).setSelectedCard(newCard);


        // at this point, in order to get any meaningful width/height 
        // of the newly created card, we have to trigger a layout pass on drawing.
        // For a correct answer we have to call applyCss() and layout(), in that order.
        // applyCss() will only work if the card is a child of a Scene, which at this point it
        // is, since it has been added to the drawing.
        drawing.getNode().applyCss();
        drawing.getNode().layout();
        Node node = newCard.getRootNode();
        Bounds cardLayoutBounds = node.getLayoutBounds();
        
        // FIXME center in scroll pane at current view
        double cardX = (drawing.getDrawingWidth() - cardLayoutBounds.getWidth()) * Math.random();
        double cardY = (drawing.getDrawingHeight() - cardLayoutBounds.getHeight()) * Math.random();

        newCard.setPosition(new Point2D(cardX, cardY));
    }

    public static void deleteCard(IDrawingBoard drawing) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Delete card");
        alert.setHeaderText("Delete selected card?");
        alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> {
            ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
            if (card != null) {
                drawing.removeCard(card);
                CardSelectionManager.getInstance(drawing).setSelectedCard(null);
            }
        });
    }

    public static void deleteConnector(IDrawingBoard drawing) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Delete connector");
        alert.setHeaderText("Delete selected connector?");
        alert.showAndWait()
        .filter(response -> response == ButtonType.OK)
        .ifPresent(response -> {
            IConnector connector = CardSelectionManager.getInstance(drawing).getSelectedConnector();
            if(connector != null) {
                drawing.removeConnector(connector);
                CardSelectionManager.getInstance(drawing).setSelectedConnector(null);
            }
        });
    }

    public static void selectedCardToFront(IDrawingBoard drawing) {
        ICard selectedCard = CardSelectionManager.getInstance(drawing).getSelectedCard();
        if (selectedCard != null) {
            selectedCard.getRootNode().toFront();
        }
    }
}
