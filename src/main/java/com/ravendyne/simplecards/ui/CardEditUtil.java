package com.ravendyne.simplecards.ui;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.simplecards.deck.card.ICardWithText;

public class CardEditUtil {

    private CardEditUtil() {}

    public static void onCardSelected(CardEditForm form, ICard card) {
        if (card != null && card instanceof ITitledCard ) {
            ITitledCard selectedCard = (ITitledCard) card;
            form.txtCardTitle.setText( selectedCard.getTitle() );
            setForCardWithText(form, card);

        } else {
            // no card has been selected
            clearForm( form );
        }
    }
    
    private static void clearForm(CardEditForm form) {
        form.txtCardTitle.setText("");
        form.txtCardText.setText("");
    }

    private static void setForCardWithText(CardEditForm form, ICard card) {
        if (card instanceof ICardWithText) {
            ICardWithText cardWithText = (ICardWithText)card;
            form.txtCardText.setText(cardWithText.getText());
        }
    }

    public static void updateCard(CardEditForm form, ICard card) {
        if (card != null && card instanceof ITitledCard) {
            ITitledCard selectedCard = ( ITitledCard ) card;
            selectedCard.setTitle(form.txtCardTitle.getText());
            updateForCardWithText( form, card );
        }
    }

    private static void updateForCardWithText(CardEditForm form, ICard card) {
        if (card instanceof ICardWithText) {
            ICardWithText cardWithText = (ICardWithText)card;
            cardWithText.setText(form.txtCardText.getText());
        }
    }

}
