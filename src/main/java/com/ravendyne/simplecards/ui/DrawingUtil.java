package com.ravendyne.simplecards.ui;

import java.io.IOException;
import java.nio.file.Path;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.simplecards.files.DrawingPersistenceManager;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;

public class DrawingUtil {

    private DrawingUtil() {}

    public static void clearDrawing(IDrawingBoard drawing) {
        drawing.clean();
        DrawingPersistenceManager.getInstance(drawing).resetCurrentFilePath();
    }

    public static void newDrawing(IDrawingBoard drawing) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("New drawing");
        alert.setHeaderText("Close current drawing and create a new one?");
        alert.showAndWait()
        .filter(response -> response == ButtonType.OK)
        .ifPresent(response -> {
            clearDrawing( drawing );
        });
    }

    public static boolean loadFromFile(IDrawingBoard drawing, Path filePath) {
        boolean isSuccess;

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Load");
        alert.setAlertType(AlertType.ERROR);
        alert.setHeaderText("Error saving file");

        try {
            DrawingPersistenceManager.getInstance(drawing).loadFrom(filePath);

            isSuccess = true;
        } catch (IOException ex) {
            alert.setContentText(ex.toString());
            isSuccess = false;
        }

        if(!isSuccess) {
            alert.showAndWait();
        }

        return isSuccess;
    }

    public static boolean saveToFile(IDrawingBoard drawing, Path filePath) {
        boolean isSuccess;

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Save");
        try {
            DrawingPersistenceManager.getInstance(drawing).saveTo(filePath);

            alert.setHeaderText("File saved");
            isSuccess = true;
        } catch (IOException ex) {
            alert.setAlertType(AlertType.ERROR);
            alert.setHeaderText("Error saving file");
            alert.setContentText(ex.toString());
            isSuccess = false;
        }
        alert.showAndWait();
        return isSuccess;
    }

}
