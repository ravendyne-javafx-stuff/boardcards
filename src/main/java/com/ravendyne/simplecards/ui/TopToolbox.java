package com.ravendyne.simplecards.ui;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.dynamofx.DynamoFxComponent;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.simplecards.SimpleCardsResources;
import com.ravendyne.simplecards.deck.SimpleCardsCreator;
import com.ravendyne.simplecards.deck.card.SCCardBuilder.DocsCardType;
import com.ravendyne.simplecards.deck.connector.SCConnectorBuilder.ConnectorType;
import com.ravendyne.simplecards.files.CardboardsFileChooser;
import com.ravendyne.simplecards.files.CardboardsFileChooser.Type;
import com.ravendyne.simplecards.files.DrawingPersistenceManager;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;

public class TopToolbox {
    private Node toolboxForm;
    
    @Inject Button btnNewGraph;
    @Inject Button btnSaveAsGraph;
    @Inject Button btnSaveGraph;
    @Inject Button btnOpenGraph;
    
    @Inject ChoiceBox<String> cbConnectorChoices;
    @Inject ChoiceBox<String> cbCardChoices;
    
    @Inject Button btnSnapshot;

    private DrawingPersistenceManager persistenceManager;
	private IDrawingBoard activeDrawing;

    public TopToolbox() {
        List<String> guiConfig = SimpleCardsResources.loader.loadTextFile("ui/top_toolbox.dynamofx");
        toolboxForm = DynamoFxComponent
            .newInstance("top toolbox", guiConfig)
            .setResourceLoader( SimpleCardsResources.loader )
            .injectInto( this )
            .build();
        
        setupUI();
    }
    
    public void setActiveDrawing(IDrawingBoard drawing) {
    	activeDrawing = drawing;
        persistenceManager = DrawingPersistenceManager.getInstance(activeDrawing);
    }
    
    private IDrawingBoard getActiveDrawing() {
    	return activeDrawing;
    }
    
    private DrawingPersistenceManager getActivePersistenceManager() {
    	return persistenceManager;
    }

    public void setupUI() {
        
        // NEW
        btnNewGraph.setOnMouseClicked((e) -> {
            DrawingUtil.newDrawing(getActiveDrawing());
        });

        // SAVE AS
        CardboardsFileChooser saveAsChooser = new CardboardsFileChooser("Save cardsfx file", toolboxForm, Type.Save, (file) -> {
            final Path filePath = Paths.get(file);
            if(getActiveDrawing() != null) {
                DrawingUtil.saveToFile(getActiveDrawing(), filePath);
            }
        });
        btnSaveAsGraph.addEventHandler(ActionEvent.ACTION, saveAsChooser);

        // OPEN EXISTING
        CardboardsFileChooser openChooser = new CardboardsFileChooser("Open cardsfx file", toolboxForm, Type.Open, (file) -> {
            final Path filePath = Paths.get(file);
            if(getActiveDrawing() != null) {
                DrawingUtil.loadFromFile(getActiveDrawing(), filePath);
            }

        });
        btnOpenGraph.addEventHandler(ActionEvent.ACTION, openChooser);

        // SAVE CURRENT
        btnSaveGraph.setOnMouseClicked((e) -> {
            if(getActiveDrawing() != null) {
                DrawingUtil.saveToFile(getActiveDrawing(), getActivePersistenceManager().getCurrentFilePath());
            }
        });
        
        dropDownsSetup();
        
        btnSnapshot.setOnMouseClicked((e) -> DrawingSnapshot.takeIt(activeDrawing));
    }

    private void dropDownsSetup() {
        Objects.requireNonNull(cbConnectorChoices, "cbConnectorChoices not injected");
        Objects.requireNonNull(cbCardChoices, "cbCardChoices not injected");

        cbCardChoices.valueProperty().addListener((observable, oldValue, newValue) -> {
            switch(newValue.trim()) {
            case "Rectangle":
                SimpleCardsCreator.getInstance().setDefaultCardType(DocsCardType.Rectangle);
                break;
            case "Circle":
                SimpleCardsCreator.getInstance().setDefaultCardType(DocsCardType.Circle);
                break;
            case "Text":
                SimpleCardsCreator.getInstance().setDefaultCardType(DocsCardType.Text);
                break;
            }
        });
        cbCardChoices.getSelectionModel().selectFirst();

        cbConnectorChoices.valueProperty().addListener((observable, oldValue, newValue) -> {
            switch(newValue.trim()) {
            case "Line":
                SimpleCardsCreator.getInstance().setDefaultConnectorType(ConnectorType.Line);
                break;
            case "LineArrow":
                SimpleCardsCreator.getInstance().setDefaultConnectorType(ConnectorType.LineArrow);
                break;
            case "Cubic":
                SimpleCardsCreator.getInstance().setDefaultConnectorType(ConnectorType.Cubic);
                break;
            case "CubicArrow":
                SimpleCardsCreator.getInstance().setDefaultConnectorType(ConnectorType.CubicArrow);
                break;
            }
        });
        cbConnectorChoices.getSelectionModel().selectFirst();
    }
    
    public Node getNode() {
        return toolboxForm;
    }
}
