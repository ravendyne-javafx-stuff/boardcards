package com.ravendyne.simplecards.ui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.simplecards.files.AppPreferences;

import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.SnapshotResult;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Callback;

public class DrawingSnapshot {
    public static void takeIt(IDrawingBoard drawing) {
        String snapshotFileName = askForName(drawing.getNode());
        if(snapshotFileName == null) {
            return;
        }

        Callback<SnapshotResult, Void> cb = (sr) -> {
            WritableImage image = sr.getImage();
            BufferedImage bimg = SwingFXUtils.fromFXImage(image, null);
            try {
                ImageIO.write(bimg, "png", new File(snapshotFileName));
                informSuccess(snapshotFileName);
            } catch (IOException ex) {
                informError(ex.toString());
            }
            return null;
        };
        drawing.getNode().snapshot(cb, null, null);
    }

    private static void informError(String errorText) {
        Platform.runLater(() -> {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Snapshot");
            alert.setHeaderText("Error saving snapshot");
            alert.setContentText(errorText);
            alert.showAndWait();
        });
    }

    private static void informSuccess(String snapshotFileName) {
        Platform.runLater(() -> {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Snapshot");
            alert.setHeaderText("Snapshot saved to '" + snapshotFileName + "'");
            alert.showAndWait();
        });
    }

    private static String askForName(Node ownerNode) {
        String filePath = null;

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Save Snapshot As");

        File initialFolder = new File(AppPreferences.INSTANCE.getLastUsedFolder());
        if( ! initialFolder.exists() ) {
            initialFolder = new File( Paths.get( "." ).toAbsolutePath().toString() );
        }
        chooser.setInitialDirectory( initialFolder );

        chooser.getExtensionFilters().addAll(
                new ExtensionFilter("All Files", "*.*"));

        File selectedFile = chooser.showSaveDialog(ownerNode.getScene().getWindow());

        if (selectedFile != null) {
            final Path selectedPath = Paths.get( selectedFile.getAbsolutePath() ).normalize();
            filePath = selectedPath.toString();
        }

        return filePath;
    }
}
