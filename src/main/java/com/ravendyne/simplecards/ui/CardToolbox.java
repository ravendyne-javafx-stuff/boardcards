package com.ravendyne.simplecards.ui;

import java.util.List;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.dynamofx.DynamoFxComponent;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.simplecards.SimpleCardsResources;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class CardToolbox {

    final double panelSizeIncrement = 50.0;

    private Node toolboxForm;

    @Inject
    VBox boxLeftBottom;

    @Inject
    Button btnNewCard;
    @Inject
    Button btnDeleteCard;
    @Inject
    Button btnExpandWidth;
    @Inject
    Button btnContractWidth;
    @Inject
    Button btnExpandHeight;
    @Inject
    Button btnContractHeight;
    @Inject
    Button btnToFront;
    @Inject
    Button btnDeleteConnector;

    // TODO get this from outside, i.e. active parentDrawing manager or tracker
    // or something
    private IDrawingBoard activeDrawing;

    public CardToolbox() {
        List<String> guiConfig = SimpleCardsResources.loader.loadTextFile("ui/card_toolbox.dynamofx");
        toolboxForm = DynamoFxComponent.newInstance("card toolbox", guiConfig).setResourceLoader(SimpleCardsResources.loader)
                .injectInto(this).build();

        setupUI();
    }

    public void setActiveDrawing(IDrawingBoard drawing) {
        activeDrawing = drawing;
    }

    private IDrawingBoard getActiveDrawing() {
        return activeDrawing;
    }

    public void setupUI() {
        btnNewCard.setOnMouseClicked((e) -> DrawingEditUtil.newCard( getActiveDrawing() ) );

        btnDeleteCard.setOnMouseClicked((e) -> DrawingEditUtil.deleteCard( getActiveDrawing() ) );
        
        btnDeleteConnector.setOnMouseClicked((e) -> DrawingEditUtil.deleteConnector( getActiveDrawing() ) );

        btnToFront.setOnMouseClicked((e) -> DrawingEditUtil.selectedCardToFront( getActiveDrawing() ) );

        btnExpandWidth.setOnMouseClicked((e) -> {
            IDrawingBoard drawing = getActiveDrawing();

            drawing.setDrawingWidth(drawing.getDrawingWidth() + panelSizeIncrement);
        });

        btnContractWidth.setOnMouseClicked((e) -> {
            IDrawingBoard drawing = getActiveDrawing();

            drawing.setDrawingWidth(drawing.getDrawingWidth() - panelSizeIncrement);
        });

        btnExpandHeight.setOnMouseClicked((e) -> {
            IDrawingBoard drawing = getActiveDrawing();

            drawing.setDrawingHeight(drawing.getDrawingHeight() + panelSizeIncrement);
        });

        btnContractHeight.setOnMouseClicked((e) -> {
            IDrawingBoard drawing = getActiveDrawing();

            drawing.setDrawingHeight(drawing.getDrawingHeight() - panelSizeIncrement);
        });
    }

    public Node getNode() {
        return toolboxForm;
    }
}
