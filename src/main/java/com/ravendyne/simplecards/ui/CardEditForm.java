package com.ravendyne.simplecards.ui;

import java.util.List;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.managers.CardSelectionManager;
import com.ravendyne.dynamofx.DynamoFxComponent;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.simplecards.SimpleCardsResources;

import javafx.beans.value.ChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class CardEditForm {

    Node editForm;

    @Inject
    VBox boxLeftTop;

    @Inject
    TextField txtCardTitle;
    @Inject
    TextArea txtCardText;
    @Inject
    Button btnUpdateCard;

    private CardSelectionManager activeSelectionManager;

    private ChangeListener<? super ICard> listener;

    public CardEditForm() {
        List<String> guiConfig = SimpleCardsResources.loader.loadTextFile("ui/card_edit_form.dynamofx");
        editForm = DynamoFxComponent.newInstance("card edit form", guiConfig).setResourceLoader(SimpleCardsResources.loader)
                .injectInto(this).build();

        setupUI();
    }

    private void setupUI() {
        HBox.setHgrow(txtCardTitle, Priority.ALWAYS);
        txtCardTitle.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(txtCardText, Priority.ALWAYS);
        txtCardText.setMaxWidth(Double.MAX_VALUE);

        listener = (observable, oldVal, newVal) -> onCardSelected( newVal );

        btnUpdateCard.setOnMouseClicked((e) -> onUpdateCard() );
    }

    private void onUpdateCard() {
        CardEditUtil.updateCard( this, activeSelectionManager.getSelectedCard() );
    }

    public void onCardSelected(ICard card) {
        CardEditUtil.onCardSelected( this, card );
    }

    public void setActiveDrawing(IDrawingBoard drawing) {
        activeSelectionManager = CardSelectionManager.getInstance(drawing);
        activeSelectionManager.addSelectedCardChangeListener(listener);
    }

    public Node getNode() {
        return editForm;
    }

}
