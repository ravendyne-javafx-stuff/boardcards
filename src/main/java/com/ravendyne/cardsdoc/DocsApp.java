package com.ravendyne.cardsdoc;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.css.Styles;
import com.ravendyne.cardsfx.managers.CardSelectionManager;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder.ConnectorType;
import com.ravendyne.cardsboard.stackofcards.StackOfCardsResources;
import com.ravendyne.cardsdoc.deck.DocsCreator;
import com.ravendyne.cardsdoc.deck.DocsDrawingPane;
import com.ravendyne.cardsdoc.deck.card.DocsCardBuilder.DocsCardType;
import com.ravendyne.dynamofx.gui.types.Inject;
import com.ravendyne.dynamofx.util.SandboxApp;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;

public class DocsApp extends SandboxApp {
    
    // TOOLBAR
    @Inject Button btnClearBoard;
    @Inject Button btnNewCard;
    @Inject Button btnDeleteCard;
    @Inject Button btnDeleteConnector;
    
    @Inject ChoiceBox<String> cbConnectorChoices;
    @Inject ChoiceBox<String> cbCardChoices;
    
    @Inject Button btnSetText;
    @Inject TextField txtCardText;
    
    // CENTER
    @Inject ScrollPane drawingScrollPane;

    // The good stuff
    IDrawingBoard drawing;

    public DocsApp() {
        title = "Docs: Graphs";
        configName = StackOfCardsResources.loader.getPackagePath(getClass()) + "docs.dynamofx";
        styleSheets.add(Styles.getDefaultCss());
        styleSheets.add(DocsApp.class.getResource("app.css").toExternalForm());
        styleSheets.add(StackOfCardsResources.class.getResource("common.css").toExternalForm());
    }

    @Override
    protected void run() {
        drawingSetup();

        buttonsSetup();
        dropDownsSetup();
    }

    private void drawingSetup() {
        drawing = new DocsDrawingPane();
        drawingScrollPane.setContent(drawing.getNode());
        
        // Change this to select different sample connector variants: Line, LineArrow, Cubic, CubicArrow
        DocsCreator.getInstance().setDefaultConnectorType(ConnectorType.CubicArrow);
        DocsCreator.getInstance().setDefaultCardType(DocsCardType.Rectangle);
    }

    private void onClearBoard() {
        CardSelectionManager.getInstance(drawing).deselectAll();
        drawing.clean();
    }

    private void onNewCard() {
        ICard newCard = drawing.getDefaultsBuilder().newDefaultCard(drawing);
        CardSelectionManager.getInstance(drawing).setSelectedCard(newCard);

        
        // at this point, in order to get any meaningful width/height 
        // of the newly created card, we have to trigger a layout pass on drawing.
        // For a correct answer we have to call applyCss() and layout(), in that order.
        // applyCss() will only work if the card is a child of a Scene, which at this point it
        // is, since it has been added to the drawing.
        drawing.getNode().applyCss();
        drawing.getNode().layout();
        Node node = newCard.getRootNode();
        Bounds cardLayoutBounds = node.getLayoutBounds();
        double cardX = (drawing.getDrawingWidth() - cardLayoutBounds.getWidth()) * Math.random();
        double cardY = (drawing.getDrawingHeight() - cardLayoutBounds.getHeight()) * Math.random();
        newCard.setPosition(new Point2D(cardX, cardY));
    }

    private void onDeleteCard() {
        ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
        if (card != null) {
            drawing.removeCard(card);
            CardSelectionManager.getInstance(drawing).setSelectedCard(null);
        }
    }

    private void onDeleteConnector() {
        IConnector connector = CardSelectionManager.getInstance(drawing).getSelectedConnector();
        if(connector != null) {
            drawing.removeConnector(connector);
            CardSelectionManager.getInstance(drawing).setSelectedConnector(null);
        }
    }
    
    private void onSetCardText() {
        ICard card = CardSelectionManager.getInstance(drawing).getSelectedCard();
        if (card != null & card instanceof ITitledCard) {
            ((ITitledCard) card).setTitle( txtCardText.getText() );
        }
    }

    private void buttonsSetup() {
        Objects.requireNonNull(btnClearBoard, "btnClearBoard not injected");
        Objects.requireNonNull(btnNewCard, "btnNewCard not injected");
        Objects.requireNonNull(btnDeleteCard, "btnDeleteCard not injected");
        Objects.requireNonNull(btnDeleteConnector, "btnDeleteConnector not injected");
        
        Objects.requireNonNull(drawingScrollPane, "drawingScrollPane not injected");

        btnClearBoard.setOnMouseClicked((e) -> onClearBoard());
        btnNewCard.setOnMouseClicked((e) -> onNewCard());
        btnDeleteCard.setOnMouseClicked((e) -> onDeleteCard());
        btnDeleteConnector.setOnMouseClicked((e) -> onDeleteConnector());
        
        btnSetText.setOnMouseClicked((e) -> onSetCardText());
    }

    private void dropDownsSetup() {
        Objects.requireNonNull(cbConnectorChoices, "cbConnectorChoices not injected");
        Objects.requireNonNull(cbCardChoices, "cbCardChoices not injected");

        cbCardChoices.valueProperty().addListener((observable, oldValue, newValue) -> {
            switch(newValue.trim()) {
            case "Rectangle":
                DocsCreator.getInstance().setDefaultCardType(DocsCardType.Rectangle);
                break;
            case "Circle":
                DocsCreator.getInstance().setDefaultCardType(DocsCardType.Circle);
                break;
            }
        });
        cbCardChoices.getSelectionModel().selectFirst();

        cbConnectorChoices.valueProperty().addListener((observable, oldValue, newValue) -> {
            switch(newValue.trim()) {
            case "Line":
                DocsCreator.getInstance().setDefaultConnectorType(ConnectorType.Line);
                break;
            case "LineArrow":
                DocsCreator.getInstance().setDefaultConnectorType(ConnectorType.LineArrow);
                break;
            case "Cubic":
                DocsCreator.getInstance().setDefaultConnectorType(ConnectorType.Cubic);
                break;
            case "CubicArrow":
                DocsCreator.getInstance().setDefaultConnectorType(ConnectorType.CubicArrow);
                break;
            }
        });
        cbConnectorChoices.getSelectionModel().selectFirst();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
