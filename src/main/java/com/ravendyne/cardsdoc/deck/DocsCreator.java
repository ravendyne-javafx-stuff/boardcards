package com.ravendyne.cardsdoc.deck;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IConnector;
import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder;
import com.ravendyne.cardsfx.samples.connectors.ConnectorBuilder.ConnectorType;
import com.ravendyne.cardsdoc.deck.card.DocsCardBuilder;
import com.ravendyne.cardsdoc.deck.card.DocsCardBuilder.DocsCardType;
import com.ravendyne.cardsdoc.deck.pin.DocsPinBuilder;

public class DocsCreator implements IDefaultsBuilder {
    private static DocsCreator instance;
    
    private static ConnectorType defaultConnectorType = ConnectorType.LineArrow;
    
    private static DocsCardType defaultCardType = DocsCardType.Rectangle;

    private DocsCreator() {}
    
    public static DocsCreator getInstance() {
        if(instance == null) {
            instance = new DocsCreator();
        }
        
        return instance;
    }

    public DocsPinBuilder pin() {
        return new DocsPinBuilder();
    }

    public DocsCardBuilder card() {
        return new DocsCardBuilder();
    }

    public ConnectorBuilder connector() {
        return new ConnectorBuilder();
    }

    public void setDefaultConnectorType(ConnectorType type) {
        defaultConnectorType = type;
    }

    public void setDefaultCardType(DocsCardType type) {
        defaultCardType = type;
    }

    @Override
    public ICard newDefaultCard(IDrawingBoard drawing) {
        ITitledCard card = card()
                .setDrawing(drawing)
                .setTitle("")
                .setType(defaultCardType)
                .build();
        card.setTitle("#" + card.getCardId());

        switch(defaultCardType) {
        case Circle:
            pin().setCard(card).setLocation(Location.Center).build();
            break;
        case Rectangle:
            pin().setCard(card).setLocation(Location.Top).build();
            pin().setCard(card).setLocation(Location.Bottom).build();
            pin().setCard(card).setLocation(Location.Right).build();
            pin().setCard(card).setLocation(Location.Left).build();
            break;
        }
        
        return card;
    }

    @Override
    public IConnector newDefaultConnector(IDrawingBoard drawing, IPin source, IPin target) {
        IConnector connector = connector()
                .setDrawingBoard(drawing)
                .setSource(source)
                .setTarget(target)
                .setType(defaultConnectorType)
                .build();
        return connector;
    }

}
