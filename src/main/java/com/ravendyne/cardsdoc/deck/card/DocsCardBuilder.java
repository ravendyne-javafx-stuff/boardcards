package com.ravendyne.cardsdoc.deck.card;

import java.util.Objects;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.ITitledCard;

public class DocsCardBuilder {
    private static final boolean bDebug = false;
    
    public static enum DocsCardType {
        Circle,
        Rectangle
    }
    
    protected IDrawingBoard pDrawing;
    protected String pTitle;
    
    DocsCardType pCardType;

    public DocsCardBuilder() {
        pCardType = DocsCardType.Rectangle;
    }

    public DocsCardBuilder setDrawing(IDrawingBoard drawing) {
        Objects.requireNonNull(drawing);
        pDrawing = drawing;
        return this;
    }

    public DocsCardBuilder setTitle(String title) {
        Objects.requireNonNull(title);
        pTitle = title;
        return this;
    }

    public DocsCardBuilder setType(DocsCardType type) {
        Objects.requireNonNull(type);
        pCardType = type;
        return this;
    }

    public ITitledCard build() {
        Objects.requireNonNull(pDrawing);
        Objects.requireNonNull(pTitle);

        ITitledCard card = null;
        switch(pCardType) {
        case Circle:
            card = new DocsCircleCard( pDrawing, pTitle );
            break;
        case Rectangle:
            card = new DocsRectangleCard( pDrawing, pTitle );
            break;
        }
        card.buildUI();
        pDrawing.addCard(card);
        if(bDebug) System.out.println("added card #" + card.getCardId());

        return card;
    }

}
