package com.ravendyne.cardsdoc.deck.card;

import com.ravendyne.cardsfx.api.IDrawingBoard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.ITitledCard;
import com.ravendyne.cardsfx.drawing.AbstractCard;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class DocsRectangleCard extends AbstractCard implements ITitledCard {
    
    Label titleLabel;
    
    IPin topPin;
    IPin bottomPin;
    IPin leftPin;
    IPin rightPin;

    DocsRectangleCard(IDrawingBoard drawing, String title) {
        super(drawing);
        
        titleLabel = new Label( title );
    }

    @Override
    protected Pane createRootUINode() {
        StackPane cardPane = new StackPane();

        cardPane.getChildren().add(getContentNode());

        cardPane.getStyleClass().add("rectangle");

        return cardPane;
    }

    @Override
    protected Node createContentUINode() {
        VBox content = new VBox();
        content.setAlignment(Pos.CENTER);
        
        content.getChildren().add(this.titleLabel);
        return content;
    }

    @Override
    public String getTitle() {
        return titleLabel.getText();
    }

    @Override
    public void setTitle(String text) {
        titleLabel.setText(text);
    }

    @Override
    public void discard() {
        super.discard();

        titleLabel = null;
    }

    @Override
    protected void removePinFromUI( IPin pin ) {
        getRootNode().getChildren().remove( pin.getRootNode() );
    }

    @Override
    protected void addPinToUI(IPin pin) {
        IPin oldPin = null;

        switch(pin.getLocation()) {
        case Top:
            oldPin = topPin;
            topPin = pin;
            break;
        case Bottom:
            oldPin = bottomPin;
            bottomPin = pin;
            break;
        case Left:
            oldPin = leftPin;
            leftPin = pin;
            break;
        case Right:
            oldPin = rightPin;
            rightPin = pin;
            break;
        case Center:
            throw new UnsupportedOperationException("Cant't add pin to card center.");
        }
        
        if(oldPin != null) {
            throw new UnsupportedOperationException("Can't add pin to already populated location");
        }

        setPinLocation(pin);

        getRootNode().getChildren().add( pin.getRootNode() );
    }

    private boolean setPinLocation(IPin pin) {
        switch (pin.getLocation()) {
        case Top:
            StackPane.setAlignment(pin.getRootNode(), Pos.TOP_CENTER);
            break;
        case Right:
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_RIGHT);
            break;
        case Bottom:
            StackPane.setAlignment(pin.getRootNode(), Pos.BOTTOM_CENTER);
            break;
        case Left:
            StackPane.setAlignment(pin.getRootNode(), Pos.CENTER_LEFT);
            break;
        case Center:
            throw new UnsupportedOperationException("Cant't set pin location to card center.");
        }
        
        return true;
    }

}
