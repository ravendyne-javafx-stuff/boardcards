package com.ravendyne.cardsdoc.deck.pin;

import java.util.Objects;

import com.ravendyne.cardsfx.api.ICard;
import com.ravendyne.cardsfx.api.IPin;
import com.ravendyne.cardsfx.api.IPin.Location;

import javafx.scene.paint.Color;

public class DocsPinBuilder {

    ICard pCard;
    Color pColor;
    Location pLocation;
    
    public DocsPinBuilder() {
        pColor = Color.web("#00a8f4");
    }
    
    public DocsPinBuilder setCard(ICard card) {
        Objects.requireNonNull(card);
        pCard = card;
        return this;
    }

    public DocsPinBuilder setColor(Color color) {
        Objects.requireNonNull(color);
        pColor = color;
        return this;
    }

    public DocsPinBuilder setColor(String colorString) {
        pColor = Color.web(colorString);
        return this;
    }

    public DocsPinBuilder setLocation(Location location) {
        Objects.requireNonNull(location);
        pLocation = location;
        return this;
    }

    public IPin build() {
        Objects.requireNonNull(pCard);
        Objects.requireNonNull(pColor);
        Objects.requireNonNull(pLocation);

        IPin pin = new DocsTransparentPin(pCard);
        pin.buildUI();
        pin.setLocation(pLocation);
        pCard.addPin(pin);

        return pin;
    }


}
