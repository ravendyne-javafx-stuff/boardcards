package com.ravendyne.cardsdoc.deck;

import com.ravendyne.cardsfx.api.IDefaultsBuilder;
import com.ravendyne.cardsfx.samples.SampleDrawingPane;

public class DocsDrawingPane extends SampleDrawingPane {
    @Override
    public IDefaultsBuilder getDefaultsBuilder() {
        return DocsCreator.getInstance();
    }
}
